package com.unisoft.bookreen1.util;

import android.app.Activity;
import android.app.Service;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.unisoft.bookreen1.ui.CalendarFragment;

import java.io.IOException;
import java.util.List;

public class GoogleCalendarUtil {
    private static final String APPLICATION_NAME = "BookreenDemo";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    public static Calendar service;

    String mToken = "";
    Object mObject;
    Activity mActivity;
    private int mTryCount = 0;

    public GoogleCalendarUtil(String _token, Object _object, Activity _activity) {
        this.mToken = _token;
        this.mObject = _object;
        this.mActivity = _activity;
    }

    private Credential getCredentials() {
        return new GoogleCredential().setAccessToken(mToken);
    }

    public void getEvents() {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = new com.google.api.client.http.javanet.NetHttpTransport();
        service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials())
                .setApplicationName(APPLICATION_NAME)
                .build();
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    // List the next 10 events from the primary calendar.
                    DateTime now = new DateTime(System.currentTimeMillis());
                    Events events = service.events().list("primary")
                            .setMaxResults(10)
                            .setTimeMin(now)
                            .setOrderBy("startTime")
                            .setSingleEvents(true)
                            .execute();
                    List<Event> items = events.getItems();
                    Log.i("event",events.toString());

                    if (mObject instanceof CalendarFragment) {
                        ((CalendarFragment) mObject).doYourGoogleEventsWorks(items);
                    }
      /*              if (items.isEmpty()) {
                        if (mObject instanceof CalendarFragment) {
                            ((CalendarFragment) mObject).doYourGoogleEventsErrorWorks();
                        }
                        System.out.println("No upcoming events found.");
                    } else {
                        System.out.println("Upcoming events");
                        for (Event event : items) {
                            DateTime start = event.getStart().getDateTime();
                            if (start == null) {
                                start = event.getStart().getDate();
                            }
                            System.out.printf("%s (%s)\n", event.getSummary(), start);
                        }
                    }*/
                } catch (Exception e) {
                    getAccessToken();
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void getAccessToken() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mActivity);

        if (account != null) {
            try {
                AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        String token = null;
                        final String SCOPES = "https://www.googleapis.com/auth/calendar.events";

                        try {
                            token = GoogleAuthUtil.getToken(
                                    mActivity, account.getAccount().name
                                    , "oauth2:" + SCOPES);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (UserRecoverableAuthException e) {
                            // startActivityForResult(e.getIntent(), 22);
                            e.printStackTrace();
                            return null;
                        } catch (GoogleAuthException e) {
                            e.printStackTrace();
                        }

                        return token;

                    }

                    @Override
                    protected void onPostExecute(String token) {
                        try {
                            if (token != null) {
                                mToken = token;
                                getEvents();
                            } else {
                                mTryCount++;
                                if (mTryCount == 3) {
                                    if (mObject instanceof CalendarFragment) {
                                        ((CalendarFragment) mObject).doGoogleSignOut();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", "Access token retrieved:" + token);
                    }

                };
                task.execute();
                // String token = GoogleAuthUtil.getToken(LoginActivity.this, account.getAccount().name, "https://www.googleapis.com/auth/calendar.events");
                //Log.v("token",token);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //      startActivity(new Intent(LoginActivity.this, MainActivity.class));

        }
    }
}