package com.unisoft.bookreen1.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.unisoft.bookreen1.model.Office;
import com.unisoft.bookreen1.model.SearchBooking;

public class MyPreferences {

    private static final String MY_PREFERENCES = "MyPrefs";

    private static final String KEY_CAMPUS = "CampusId";
    private static final String KEY_CUSTOM_CAMPUS = "CustomCampusId";
    private static final String KEY_BUILDING = "BuildingId";
    private static final String KEY_CUSTOM_BUILDING = "CustomBuildingId";
    private static final String KEY_FLOOR = "FloorId";
    private static final String KEY_CUSTOM_FLOOR = "Custom_FloorId";
    private static final String KEY_GOOGLE_CALENDAR_TOKEN = "google_token";

    public static void SaveGoogleCalendarTokenPref(Context _context, String _token) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_GOOGLE_CALENDAR_TOKEN, _token);
        editor.apply();
    }

    public static String GetGoogleCalendarTokenPref(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getString(KEY_GOOGLE_CALENDAR_TOKEN, "0");
    }

    public static void SaveCampusPref(Context _context, int _campusId) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_CAMPUS, _campusId);
        editor.apply();
    }

    public static void SaveCustomCampusPref(Context _context, int _campusId) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_CUSTOM_CAMPUS, _campusId);
        editor.apply();
    }

    public static void SaveBuildingPref(Context _context, int _buildingId) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_BUILDING, _buildingId);
        editor.apply();
    }

    public static void SaveCustomBuildingPref(Context _context, int _buildingId) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_CUSTOM_BUILDING, _buildingId);
        editor.apply();
    }

    public static void SaveFloorPref(Context _context, int _floorId) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_FLOOR, _floorId);
        editor.apply();
    }

    public static void SaveCustomFloorPref(Context _context, int _floorId) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_CUSTOM_FLOOR, _floorId);
        editor.apply();
    }


    public static int GetCampusPref(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_CAMPUS, 0);
    }

    public static int GetCustomCampusPref(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_CUSTOM_CAMPUS, 0);
    }


    public static int GetBuildingPref(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_BUILDING, 0);
    }

    public static int GetCustomBuildingPref(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_CUSTOM_BUILDING, 0);
    }

    public static int GetFloorPref(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_FLOOR, 0);
    }

    public static int GetCustomFloorPref(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_CUSTOM_FLOOR, 0);
    }

    public static Office GetMyOffice(Context _context) {
        return new Office(GetCampusPref(_context), GetBuildingPref(_context), GetFloorPref(_context));
    }

    public static Office GetCustomOffice(Context _context) {
        return new Office(GetCustomCampusPref(_context), GetCustomBuildingPref(_context), GetCustomFloorPref(_context));
    }

    public static void saveJsonObject(Context c, Object modal, String key) {
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                c.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        Gson gson = new Gson();
        String jsonObject = gson.toJson(modal);
        prefsEditor.putString(key, jsonObject);
        prefsEditor.commit();

    }

    public static Object getJsonObject(Context c, String key) {

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                c.getApplicationContext());
        String json = appSharedPrefs.getString(key, "");
        Gson gson = new Gson();
        SearchBooking selectedUser = gson.fromJson(json, SearchBooking.class);
        return selectedUser;
    }
}