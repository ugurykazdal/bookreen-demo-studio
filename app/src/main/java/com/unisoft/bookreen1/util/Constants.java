package com.unisoft.bookreen1.util;

import com.unisoft.bookreen1.model.Occasion;
import com.unisoft.bookreen1.ui.MainActivity;

import java.util.HashSet;

public class Constants {
    public static String SESSION = "";
    public static int SELECTED_DAY = 1;
    public static int SELECTED_MONTH = 1;
    public static int SELECTED_YEAR = 2020;
    public static String COOKIE = "";
    public static HashSet<String> cookies;
    public static Occasion selectedOccasion;
    public static MainActivity mainActivity;

    public static int SELECTED_BOOK_MIN = 30;

    public static String SELECTED_BOOK_DATE = "";
    public static String SELECTED_BOOK_STIME = "";
    public static String SELECTED_BOOK_ETIME = "";

    public static boolean OTHER_BOOK_SELECTED = false;

    public static boolean FROM_OCCASION_LIST = false;

    public static int DEFAULT_OCCASION_MINUTE = 30;

    public static boolean IS_MY_OFFICE = true;


}
