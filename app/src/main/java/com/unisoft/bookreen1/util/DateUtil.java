
package com.unisoft.bookreen1.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    static final long ONE_MINUTE_IN_MILLIS = 60000;

    public static Date stringToDateObject3(String _date) {
        if (_date == null) {
            return null;
        }

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.UK);
        Date d1 = null;
        try {
            d1 = format.parse(_date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d1;
    }

    public static Date parseEDate(String inputDate) {
        Date mParsedDate;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            mParsedDate = inputFormat.parse(inputDate);
            return mParsedDate;
        } catch (Exception e) {
            return new Date();
        }
    }

    public static String formatUpcomingDates(String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("MMM dd");

        mParsedDate = inputFormat.parse(inputDate);
        mOutputDateString = outputFormat.format(mParsedDate);
        return mOutputDateString;
    }

    public static String formatGoogleCalendarDate(String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("EEEE dd MMMM");

        mParsedDate = inputFormat.parse(inputDate);
        mOutputDateString = outputFormat.format(mParsedDate);
        return mOutputDateString;
    }

    public static String formatDateOccasion(String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");

        mParsedDate = inputFormat.parse(inputDate);
        mOutputDateString = outputFormat.format(mParsedDate);
        return mOutputDateString;
    }

    public static String formatTimeOccasion(String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        DateFormat inputFormat = new SimpleDateFormat("HH:mm");
        DateFormat outputFormat = new SimpleDateFormat("HH:mm");

        mParsedDate = inputFormat.parse(inputDate);
        mOutputDateString = outputFormat.format(mParsedDate);
        return mOutputDateString;
    }

    public static String getCurrentDate() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
    }

    public static String getCurrentDay() {
        return new SimpleDateFormat("dd", Locale.getDefault()).format(new Date());
    }

    public static String getCurrentMonth() {
        return new SimpleDateFormat("MM", Locale.getDefault()).format(new Date());
    }

    public static String getCurrentYear() {
        return new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
    }

    public static String getCurrentTime(int min) {
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        Date afterAddingTenMins = new Date(t + (min * ONE_MINUTE_IN_MILLIS));
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(afterAddingTenMins);
    }

    public static String DateTimeAddZero(String str) {
        return str.length() == 1 ? "0" + str : str;
    }

}