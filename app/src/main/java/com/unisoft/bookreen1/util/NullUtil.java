package com.unisoft.bookreen1.util;

import android.view.View;

public class NullUtil {
    public static boolean isTagTrue(View _view) {
        if (_view == null) return false;
        if (_view.getTag() == null) return false;
        return (boolean) _view.getTag();
    }
}
