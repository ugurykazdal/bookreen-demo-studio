package com.unisoft.bookreen1;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.unisoft.bookreen1.network.rest.RestClient;

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        new RestClient();
        Stetho.initializeWithDefaults(this);

    }
}
