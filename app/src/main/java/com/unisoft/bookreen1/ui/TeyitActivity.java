package com.unisoft.bookreen1.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Reservation;
import com.unisoft.bookreen1.network.Repository;
import com.unisoft.bookreen1.network.Response;
import com.unisoft.bookreen1.util.Constants;


public class TeyitActivity extends AppCompatActivity {
    private Reservation mReservation;
    private Repository mRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teyit);
        mReservation = new Reservation();
        mRepository = new Repository(this, TeyitActivity.this);
        mReservation.setLocationID(Integer.parseInt(Constants.selectedOccasion.getID()));
        mReservation.setDate(Constants.SELECTED_BOOK_DATE);
        mReservation.setSTime(Constants.SELECTED_BOOK_STIME);
        mReservation.setETime(Constants.SELECTED_BOOK_ETIME);


        findViewById(R.id.img_occ_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mReservation.setSubject(((EditText) findViewById(R.id.tv_teyit_subject)).getText().toString());
                mRepository.SaveReservation("token", mReservation);
            }
        });

        findViewById(R.id.img_occ_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void doSaveReservationWorks(Response<Reservation> data) {
        if (data.isStatus()) {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(R.layout.fragment_thank_you);
            bottomSheetDialog.findViewById(R.id.btn_thankyou_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                    finish();
                }
            });
            bottomSheetDialog.show();

            //finish();
        } else {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(R.layout.fragment_thank_you);

            bottomSheetDialog.findViewById(R.id.img_thankyou_status).setBackgroundResource(R.drawable.wrong_not_success);
            bottomSheetDialog.findViewById(R.id.btn_thankyou_ok).setBackgroundColor(getResources().getColor(R.color.color_error));
            ((TextView) bottomSheetDialog.findViewById(R.id.tv_thankyou_title)).setText(data.getMessage());

            bottomSheetDialog.findViewById(R.id.btn_thankyou_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                    finish();
                }
            });
            bottomSheetDialog.show();
        }

    }
}