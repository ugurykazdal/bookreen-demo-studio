package com.unisoft.bookreen1.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.unisoft.bookreen1.util.Constants;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.util.DateUtil;

import java.text.ParseException;

public class FindDateSelectActivity extends AppCompatActivity {
    ImageView btnUpDay, btnDownDay, btnUpMonth, btnDownMonth, btnUpYear, btnDownYear;
    TextView tvPickedDay, tvPickedMonth, tvPickedYear;
    int mDayValue = 7, mMonthValue = 9, mYearValue = 2020;

    ImageView btnUpFromDate, btnDownFromDate, btnUpToDate, btnDownToDate;
    TextView tvPickedFromDate, tvPickedToDate;
    int mFromHourValue = 18, mFromHourMinute = 60, mToHourValue = 18, mToHourMinute = 30;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_date_select);

        setDateSelectValues();
        setTimeSelectValues();


        try {
            mDayValue = Integer.parseInt(DateUtil.getCurrentDay());
            mMonthValue = Integer.parseInt(DateUtil.getCurrentMonth());
            mYearValue = Integer.parseInt(DateUtil.getCurrentYear());

        } catch (Exception e) {
            e.printStackTrace();
        }
        tvPickedDay.setText(DateUtil.getCurrentDay());
        tvPickedMonth.setText(monthStr());
        tvPickedYear.setText(DateUtil.getCurrentYear());

        String timeFromDefault = DateUtil.getCurrentTime(0);
        String timeToDefault = DateUtil.getCurrentTime(Constants.SELECTED_BOOK_MIN);

        try {

            String parsedFromH = timeFromDefault.substring(0, timeFromDefault.indexOf(":"));
            String parsedFromM = timeFromDefault.substring(timeFromDefault.indexOf(":") + 1);
            mFromHourValue = Integer.parseInt(parsedFromH);
            mFromHourMinute = Integer.parseInt(parsedFromM);
            tvPickedFromDate.setText(getFinalDateOrTimeStr(getHourValue(Math.abs(mFromHourValue), true)) + ":" + getFinalDateOrTimeStr(mFromHourMinute));

            String parsedToH = timeToDefault.substring(0, timeFromDefault.indexOf(":"));
            String parsedToM = timeToDefault.substring(timeFromDefault.indexOf(":") + 1);
            mToHourValue = Integer.parseInt(parsedToH);
            mToHourMinute = Integer.parseInt(parsedToM);
            tvPickedToDate.setText(getFinalDateOrTimeStr(getHourValue(Math.abs(mToHourValue), false)) + ":" + getFinalDateOrTimeStr(mToHourMinute));

        } catch (Exception e) {

        }

        DateUtil.getCurrentTime(Constants.SELECTED_BOOK_MIN);

        findViewById(R.id.imgBtn_findDateSelect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
/*                String date = mYearValue + "-" + DateUtil.DateTimeAddZero(String.valueOf(mMonthValue)) + "-" + DateUtil.DateTimeAddZero(String.valueOf(mDayValue));
                String sTime = DateUtil.DateTimeAddZero(String.valueOf(mFromHourValue)) + ":" + DateUtil.DateTimeAddZero(String.valueOf(mFromHourMinute));
                String eTime = DateUtil.DateTimeAddZero(String.valueOf(mToHourValue)) + ":" + DateUtil.DateTimeAddZero(String.valueOf(mToHourMinute));*/
                String date = "";
                String sTime = "";
                String eTime = "";
                try {
                    date = DateUtil.formatDateOccasion(mYearValue + "-" + mMonthValue + "-" + mDayValue);
                    sTime = DateUtil.formatTimeOccasion(mFromHourValue + ":" + mFromHourMinute);
                    eTime = DateUtil.formatTimeOccasion(mToHourValue + ":" + mToHourMinute);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent();

                intent.putExtra("selectedDate", date);
                intent.putExtra("selectedSTime", sTime);
                intent.putExtra("selectedEDate", eTime);

                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }


    private void setTimeSelectValues() {
        btnUpFromDate = findViewById(R.id.btn_fromInc);
        btnDownFromDate = findViewById(R.id.btn_fromDec);
        btnUpToDate = findViewById(R.id.btn_toInc);
        btnDownToDate = findViewById(R.id.btn_toDec);

        tvPickedFromDate = findViewById(R.id.tv_fromHour);
        tvPickedToDate = findViewById(R.id.tv_toHour);

        btnUpFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFromHourMinute += 1;
                setDateFromTextViews(true);
            }
        });

        btnDownFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFromHourMinute -= 1;
                setDateFromTextViews(false);
            }
        });

        btnUpToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mToHourMinute += 1;
                setDateToTextViews(true);
            }
        });

        btnDownToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mToHourMinute -= 1;
                setDateToTextViews(false);
            }
        });
    }

    private void setDateFromTextViews(boolean isUp) {
        int minVal = getMinuteValue(Math.abs(mFromHourMinute), true, isUp);
        tvPickedFromDate.setText(getFinalDateOrTimeStr(getHourValue(Math.abs(mFromHourValue), true)) + ":" + getFinalDateOrTimeStr(minVal));
    }

    private void setDateToTextViews(boolean isUp) {
        int minVal = getMinuteValue(Math.abs(mToHourMinute), false, isUp);
        tvPickedToDate.setText(getFinalDateOrTimeStr(getHourValue(Math.abs(mToHourValue), false)) + ":" + getFinalDateOrTimeStr(minVal));
    }

    private String getFinalDateOrTimeStr(int val) {
        if (String.valueOf(val).length() == 1) {
            return "0" + val;
        }

        return String.valueOf(val);
    }

    private int getHourValue(int val, boolean isFrom) {
        if (val > 24) {
            if (isFrom) {
                mFromHourValue = 1;
            } else {
                mToHourValue = 1;
            }
            return 1;
        }

        if (val < 1) {
            if (isFrom) {
                mFromHourValue = 24;
            } else {
                mToHourValue = 24;
            }
            return 0;
        }
        if (val == 24)
            val = 0;

        return val;
    }

    private int getMinuteValue(int val, boolean isFrom, boolean isUp) {
        if (isUp) {

            val = Math.abs(val % 60);

            if (val == 0) {
                if (isFrom) {
                    mFromHourMinute = 60;
                    mFromHourValue++;
                } else {
                    mToHourMinute = 60;
                    mToHourValue++;
                }
                return 0;
            } else {
                if (isFrom)
                    mFromHourMinute = val;
                else
                    mToHourMinute = val;
                return val;
            }
        } else {
            val = Math.abs(val % 60);
            if (val == 0) {
                if (isFrom) {
                    mFromHourMinute = 60;
                } else {
                    mToHourMinute = 60;
                }
                return 0;
            } else if (val == 45) {
                if (isFrom) {
                    mFromHourMinute = val;
                    mFromHourValue--;
                } else {
                    mToHourMinute = val;
                    mToHourValue--;
                }
                return val;

            } else {
                if (isFrom)
                    mFromHourMinute = val;
                else
                    mToHourMinute = val;
                return val;
            }
        }

    }

    private void setDateSelectValues() {
        btnUpDay = findViewById(R.id.btnUp_day);
        btnDownDay = findViewById(R.id.btnDown_day);
        btnUpMonth = findViewById(R.id.btn_monthUp);
        btnDownMonth = findViewById(R.id.btn_monthDown);
        btnUpYear = findViewById(R.id.btn_yearUp);
        btnDownYear = findViewById(R.id.btn_yearDown);

        tvPickedDay = findViewById(R.id.tv_selectDay);
        tvPickedMonth = findViewById(R.id.tv_selectMonth);
        tvPickedYear = findViewById(R.id.tv_selectYear);

        findViewById(R.id.imgBtn_findDateSelect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FindDateSelectActivity.this, ThankYouActivity.class));
            }
        });

        btnUpDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDayValue++;
                setTextView();
            }
        });

        btnDownDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDayValue--;
                setTextView();
            }
        });

        btnUpMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMonthValue++;
                setTextView();
            }
        });

        btnDownMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMonthValue--;
                setTextView();
            }
        });

        btnUpYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mYearValue++;
                setTextView();
            }
        });

        btnDownYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mYearValue--;
                setTextView();
            }
        });
    }

    private void setTextView() {

        Constants.SELECTED_DAY = getmDayValue();
        tvPickedDay.setText(String.valueOf(getmDayValue()));
        tvPickedMonth.setText(monthStr());
        tvPickedYear.setText(String.valueOf(mYearValue));

        Constants.SELECTED_DAY = mDayValue;
        Constants.SELECTED_MONTH = mMonthValue;
        Constants.SELECTED_YEAR = mYearValue;
    }

    private int getmDayValue() {
        if (mDayValue < 1) {
            mDayValue = 31;
            return 31;
        }
        if (mDayValue > 31) {
            mDayValue = 1;
            return 1;
        }

        return mDayValue;
    }

    private String monthStr() {
        if (mMonthValue > 12) mMonthValue = 1;
        if (mMonthValue < 1) mMonthValue = 12;
        switch (mMonthValue) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "January";
        }
    }
}
