package com.unisoft.bookreen1.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Occasion;
import com.unisoft.bookreen1.model.Office;
import com.unisoft.bookreen1.model.SearchBooking;
import com.unisoft.bookreen1.network.Repository;
import com.unisoft.bookreen1.ui.adapter.OccasionAdapter;
import com.unisoft.bookreen1.util.Constants;
import com.unisoft.bookreen1.util.DateUtil;
import com.unisoft.bookreen1.util.MyPreferences;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class OccasionFragment extends Fragment {
    private RecyclerView.Adapter mAdapter;
    private List<Occasion> mOccasions;
    private RecyclerView occasionRecycler;
    private int REQUEST_CODE_FIND = 11;
    private SearchBooking mSearchBooking = new SearchBooking();
    private Chip mChipmin, mChipDeskRoom, mChipOffice;
    private TextView mTv_occasion;
    private ImageView mImageBtnCustomOffice;
    private ProgressBar pbOccasion;
    private LinearLayout ly_noContent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_occasions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mChipmin = view.findViewById(R.id.chip_min);
        mChipDeskRoom = view.findViewById(R.id.chip_type);
        mChipOffice = view.findViewById(R.id.chip_custom);
        mTv_occasion = view.findViewById(R.id.tv_occasion_date);
        mImageBtnCustomOffice = view.findViewById(R.id.btn_customOffice);
        pbOccasion = view.findViewById(R.id.pb_occasionlist);
        ly_noContent = view.findViewById(R.id.ly_nocontentOccasions);
        occasionRecycler = view.findViewById(R.id.rec_occasions);
        occasionRecycler.setHasFixedSize(true);
        occasionRecycler.setLayoutManager(new LinearLayoutManager(getContext()));


        setUserVisibleHint(true);
        ((MainActivity) getActivity()).occasionFragment = this;

        getDefaultSearchObject();
        doRequestSearchBooking();

        if (Constants.SELECTED_BOOK_MIN == 0) {
            mChipmin.setVisibility(View.GONE);
        }

        if (Constants.OTHER_BOOK_SELECTED) {
            mChipmin.setVisibility(View.GONE);
            mTv_occasion.setText(Constants.SELECTED_BOOK_DATE + ", " + Constants.SELECTED_BOOK_STIME + " to " + Constants.SELECTED_BOOK_ETIME + ".");

        } else {
            mTv_occasion.setText("Today, " + DateUtil.getCurrentTime(0) + " to " + DateUtil.getCurrentTime(Constants.SELECTED_BOOK_MIN) + ".");

        }


        mChipmin.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                getDefaultSearchObject();
                doRequestSearchBooking();
            }
        });


        mChipDeskRoom.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                getDefaultSearchObject();
                doRequestSearchBooking();
            }
        });

        mImageBtnCustomOffice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), FindActivity.class), REQUEST_CODE_FIND);
            }
        });


/*        mChipOffice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), FindActivity.class), REQUEST_CODE_FIND);
            }
        });*/


    }

    private void setRecycleAdapter(List<Occasion> upcomingEvents) {
        mAdapter = new OccasionAdapter(upcomingEvents, ((MainActivity) getActivity()));
        mAdapter.setHasStableIds(true);
        occasionRecycler.setAdapter(mAdapter);
    }

    public void doGetRoomsAndDesksWorks(List<Occasion> data) {
        mOccasions = data;
        if (data == null || data.isEmpty())
            ly_noContent.setVisibility(View.VISIBLE);

        setRecycleAdapter(mOccasions);
        pbOccasion.setVisibility(View.GONE);
        occasionRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mChipmin.setText(Constants.SELECTED_BOOK_MIN + " min");
        } else {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_FIND) {
            if (resultCode == RESULT_OK) {
                mSearchBooking = new SearchBooking();
                mChipmin.setText(Constants.SELECTED_BOOK_MIN + " min");


                int campusId = data.getIntExtra("campusId", 0);
                int buildingId = data.getIntExtra("buildingId", 0);
                int floorId = data.getIntExtra("floorId", 0);
                String planDate = data.getStringExtra("date");
                String sTime = data.getStringExtra("sTime");
                String eTime = data.getStringExtra("eTime");
                int capacity = data.getIntExtra("capacity", 0);
                int locType = data.getIntExtra("locationType", 0);
                int layoutId = data.getIntExtra("layoutId", 0);
                ArrayList<Integer> amenities = data.getIntegerArrayListExtra("amenities");

                mSearchBooking.setEquipmentIds(amenities);
                mSearchBooking.setLayoutId(layoutId);
                mSearchBooking.setCapacity(capacity);

                if (locType == FindActivity.MAHAL_TYPE_DESK)
                    mChipDeskRoom.setText("Desk");
                else {
                    mChipDeskRoom.setText("Room");
                }


                if (planDate.equals("")) {
                    mSearchBooking.setPlanDate(DateUtil.getCurrentDate());
                } else {
                    mSearchBooking.setPlanDate(planDate);
                }

                if (sTime.equals("")) {
                    mSearchBooking.setSTime(DateUtil.getCurrentTime(0));
                    if (mChipmin.getVisibility() == View.GONE) {
                        mSearchBooking.setETime("23:59");
                        mTv_occasion.setText("Today, All day.");

                    } else {
                        mSearchBooking.setETime(DateUtil.getCurrentTime(Constants.SELECTED_BOOK_MIN));
                        mTv_occasion.setText("Today, " + DateUtil.getCurrentTime(0) + " to " + DateUtil.getCurrentTime(Constants.SELECTED_BOOK_MIN) + ".");

                    }
                } else {
                    mSearchBooking.setSTime(sTime);
                    mSearchBooking.setETime(eTime);
                    mTv_occasion.setText(Constants.SELECTED_BOOK_DATE + ", " + Constants.SELECTED_BOOK_STIME + " to " + Constants.SELECTED_BOOK_ETIME + ".");
                }

                mSearchBooking.setLocationType(locType);
                mSearchBooking.setOffice(new Office(campusId, buildingId, floorId));

                int campusIdMyOffice = MyPreferences.GetCampusPref(getContext());
                int buildingIdMyOffice = MyPreferences.GetBuildingPref(getContext());
                int floorIdMyOffice = MyPreferences.GetFloorPref(getContext());
                if (campusIdMyOffice == campusId && buildingIdMyOffice == buildingId && floorIdMyOffice == floorId) {
                    mChipOffice.setText("My Office");
                } else {
                    mChipOffice.setText("Custom Office");
                }

                doRequestSearchBooking();
            } else {
                mChipOffice.setText("My Office");
                getDefaultSearchObject();
                doRequestSearchBooking();
            }
        }
    }

    private void doRequestSearchBooking() {
        MyPreferences.saveJsonObject(getContext(), mSearchBooking, "mySearchBooking");

        Constants.IS_MY_OFFICE = !mChipOffice.getText().toString().contains("Custom");
        pbOccasion.setVisibility(View.VISIBLE);
        ly_noContent.setVisibility(View.GONE);
        occasionRecycler.setVisibility(View.GONE);
        Constants.SELECTED_BOOK_DATE = mSearchBooking.getPlanDate();
        Constants.SELECTED_BOOK_STIME = mSearchBooking.getSTime();
        Constants.SELECTED_BOOK_ETIME = mSearchBooking.getETime();
        new Repository(getContext(), OccasionFragment.this).SearchBooking("token", mSearchBooking);
    }

    private void getDefaultSearchObject() {

        if (mChipmin.getVisibility() == View.GONE) {
            if (!Constants.OTHER_BOOK_SELECTED) {
                mSearchBooking.setETime("23:59");
                mTv_occasion.setText("Today, All day.");
                Constants.SELECTED_BOOK_MIN = 0;
            }
        } else {
            mSearchBooking.setETime(DateUtil.getCurrentTime(Constants.SELECTED_BOOK_MIN));
        }

        if (mChipDeskRoom.getVisibility() == View.GONE) {
            mSearchBooking.setLocationType(0);
        } else {
            if (mChipDeskRoom.getText().toString().startsWith("D"))
                mSearchBooking.setLocationType(FindActivity.MAHAL_TYPE_DESK);
            else
                mSearchBooking.setLocationType(FindActivity.MAHAL_TYPE_MEETING);
        }

        int campusId = MyPreferences.GetCampusPref(getContext());
        int buildingId = MyPreferences.GetBuildingPref(getContext());
        int floorId = MyPreferences.GetFloorPref(getContext());

        mSearchBooking.setCapacity(0);
        mSearchBooking.setPlanDate(DateUtil.getCurrentDate());
        mSearchBooking.setEquipmentIds(new ArrayList<>());
        mSearchBooking.setLayoutId(0);
        mSearchBooking.setSTime(DateUtil.getCurrentTime(0));
        mSearchBooking.setOffice(new Office(campusId, buildingId, floorId));
    }
}