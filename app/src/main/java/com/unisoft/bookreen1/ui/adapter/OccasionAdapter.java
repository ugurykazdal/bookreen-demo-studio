package com.unisoft.bookreen1.ui.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.unisoft.bookreen1.util.Constants;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Occasion;
import com.unisoft.bookreen1.ui.MainActivity;
import com.unisoft.bookreen1.ui.PinFragment;
import com.unisoft.bookreen1.ui.TeyitActivity;
import com.unisoft.bookreen1.util.MyPreferences;

import java.util.List;

public class OccasionAdapter extends RecyclerView.Adapter<OccasionAdapter.MyViewHolder> {
    private List<Occasion> mDataset;
    private MainActivity mMainActivity;
    View v;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tv_mahalType;
        TextView tv_mahal;
        TextView tv_availability;
        TextView tv_date;
        ConstraintLayout consRoot;
        ImageView imageView;
        ImageView imgMahal;


        MyViewHolder(View v) {
            super(v);
            tv_mahal = v.findViewById(R.id.tv_occassion_mahal);
            tv_mahalType = v.findViewById(R.id.tv_occasion_mahalType);
            tv_availability = v.findViewById(R.id.tv_occasion_availability);
            tv_date = v.findViewById(R.id.tv_occasion_date);
            consRoot = v.findViewById(R.id.cons_occasion_root);
            imageView = v.findViewById(R.id.img_occasion_info);
            imgMahal = v.findViewById(R.id.img_ocaasion_mahal);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OccasionAdapter(List<Occasion> myDataset, MainActivity mainActivity) {
        mDataset = myDataset;
        mMainActivity = mainActivity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OccasionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
         v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_occasions, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tv_mahal.setText(mDataset.get(position).getBuildingName() + "/" + mDataset.get(position).getFloorName());
        holder.tv_date.setText(mDataset.get(position).getCapacity());
        holder.tv_mahalType.setText(mDataset.get(position).getName());

        if (!mDataset.get(position).getName().contains("Desk")) {
            v.setBackgroundColor(mMainActivity.getResources().getColor(R.color.borderBookreen2));
        }

        try {
            holder.imgMahal.setImageResource(Layouts.getLayoutDrawable(mDataset.get(position).getLayout()));

        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.selectedOccasion = mDataset.get(position);

                holder.itemView.getContext().startActivity(new Intent(holder.itemView.getContext(), TeyitActivity.class));
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.FROM_OCCASION_LIST = true;
                int floorId = 0;
                if (Constants.IS_MY_OFFICE)
                    floorId = MyPreferences.GetMyOffice(holder.consRoot.getContext()).getFloorID();
                else
                    floorId = MyPreferences.GetCustomOffice(holder.consRoot.getContext()).getFloorID();

                PinFragment.URL = "http://85.105.163.29/backend/plugins/mapplic/main/2apartment.php?floorId=" + floorId + "&roomId=" + mDataset.get(position).getID();
                //  mMainActivity.loadFragment(new PinFragment());
                mMainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_pin);

            }
        });

/*        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tv_mahal.setText(mDataset.get(position).getMeetingRoomName());
        holder.tv_subject.setText(mDataset.get(position).getSubjet());
        try {
            holder.tv_date.setText(mDataset.get(position).getSTime().substring(0, 5) + " : " + mDataset.get(position).getETime().substring(0, 5) + " , " + DateUtil.formatUpcomingDates(mDataset.get(position).getPlanDate()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tv_duzenleyen.setText(mDataset.get(position).getOrganizer());*/


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}