package com.unisoft.bookreen1.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputLayout;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;

import com.google.api.services.calendar.model.EventDateTime;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.util.GoogleCalendarUtil;

import java.io.IOException;
import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

public class UpdateEvent extends AppCompatActivity {


    private TextInputLayout et_subject;
    private Button btn_update;
    private Event event;
    private String eventId;
    private TextInputLayout mStartInputLayout;
    private TextInputLayout mEndInputLayout;
    private TextInputLayout mAttendees;
    private TextInputLayout et_body;
    private EditTextDateTimePicker mStartPicker;
    private EditTextDateTimePicker mEndPicker;
    private TextView startText,endText;
    private Boolean allday=false;
    private String mTimeZone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_event);

        mTimeZone="Europe/Istanbul";
        eventId=getIntent().getStringExtra("eventId");

        et_subject=findViewById(R.id.textInputLy_updateEventSubject);
        btn_update=findViewById(R.id.btn_updateEvent);
        et_body = findViewById(R.id.neweventbody);
        mAttendees =findViewById(R.id.neweventattendees);
        startText=findViewById(R.id.tv_start);
        endText=findViewById(R.id.tv_end);

        SwitchMaterial sw = (SwitchMaterial) findViewById(R.id.simpleSwitch);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mStartInputLayout.setVisibility(View.GONE);
                    mEndInputLayout.setVisibility(View.GONE);
                    startText.setVisibility(View.GONE);
                    endText.setVisibility(View.GONE);
                    allday=true;

                } else {
                    mStartInputLayout.setVisibility(View.VISIBLE);
                    mEndInputLayout.setVisibility(View.VISIBLE);
                    startText.setVisibility(View.VISIBLE);
                    endText.setVisibility(View.VISIBLE);
                    allday=false;

                }
            }
        });


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    event = GoogleCalendarUtil.service.events().get("primary", eventId).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                setTextView(event);

            }
        }).start();

        ZoneId userTimeZone = GraphToIana.getZoneIdFromWindows(mTimeZone);
        mStartInputLayout = findViewById(R.id.neweventstartdatetime);
        mStartPicker = new EditTextDateTimePicker(this,
                mStartInputLayout.getEditText(),
                userTimeZone);

        mEndInputLayout = findViewById(R.id.neweventenddatetime);
        mEndPicker = new EditTextDateTimePicker(this,
                mEndInputLayout.getEditText(),
                userTimeZone);









        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            String sub= et_subject.getEditText().getText().toString();
                            String body = et_body.getEditText().getText().toString();
                            ZonedDateTime startDateTime2 = mStartPicker.getZonedDateTime();
                            String startDateTime1=startDateTime2.toString().substring(0,16);
                            ZonedDateTime endDateTime2 = mEndPicker.getZonedDateTime();
                            String endDateTime1=endDateTime2.toString().substring(0,16);
                            // Retrieve the event from the API

                            event.setSummary(sub);
                            event.setDescription(body);
                            DateTime startDateTime = new DateTime(startDateTime1+":00");
                            EventDateTime start = new EventDateTime()
                                    .setDateTime(startDateTime);
                            event.setStart(start);

                            DateTime endDateTime = new DateTime(endDateTime1+":00");
                            EventDateTime end = new EventDateTime()
                                    .setDateTime(endDateTime);
                            event.setEnd(end);


                            GoogleCalendarUtil.service.events().update("primary", event.getId(), event).execute();


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                Toast.makeText(getApplicationContext()," Update Gerçekleşti ", Toast.LENGTH_SHORT).show();

            }
        });



    }

    private void setTextView(Event event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    et_subject.getEditText().setText(event.getSummary());
                    et_body.getEditText().setText(event.getDescription());

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }


}