package com.unisoft.bookreen1.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Guideline;

import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Office;
import com.unisoft.bookreen1.model.SearchBooking;
import com.unisoft.bookreen1.ui.adapter.Layouts;
import com.unisoft.bookreen1.util.Constants;
import com.unisoft.bookreen1.util.DateUtil;
import com.unisoft.bookreen1.util.MyPreferences;

import java.util.ArrayList;

public class FindActivity extends AppCompatActivity {
    public static final int MAHAL_TYPE_DESK = 3;
    public static final int MAHAL_TYPE_MEETING = 4;


    private ArrayList<Integer> mSelectedAmenities = new ArrayList<>();
    private Integer mSelectedLayout = 0;
    private int mSelectedCampusId = 0;
    private int mSelectedBuildingId = 0;
    private int mSelectedFloorId = 0;
    private String mSelectedDate = "";
    private String mSelectedSTime = "";
    private String mSelectedETime = "";
    private int mSelectedCapacity = 0;
    private int mSelectedLocationType = 0;
    private SearchBooking mSearchBooking;

    private ImageView btn_room, btnDesk;
    private TextView tv_available15, tv_available30, tv_available1Hr, tv_available2Hr, tv_availableOther;
    private TextView tv_locType_nearby, tv_locType_myoffice, tv_locType_customOffice;
    private LinearLayout ly_capacity1_4, ly_capacity5_10, ly_capacity11_20, ly_capacity20;
    private ImageView img_ameniPhone, img_ameniSesSistemi, img_ameniMonitor, img_ameniAkilliTahta, img_ameniKlima, img_ameniPerde, img_ameniWifi, img_ameniKablosuz, img_ameniYazi, img_ameniVideo;
    private ImageView img_sitYuvMasa, img_sitOval, img_sitKare, img_sitUMasa, img_sitDrtgenMasa, img_sitAmfi, img_sitSinif, img_sitTekCalisma, img_sitSiraCalisma, img_sitKupCalisma, img_sitUcgenCalisma, img_sitOvalSiraCalisma;

    private static final int REQUEST_CUSTOM_OFFICE = 111;
    private static final int REQUEST_CUSTOM_DATE = 112;
    private boolean fromCustomOfficeHistory = false;
    private boolean fromAvailOtherHistory = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_find);
        setviewResources();


        findViewById(R.id.btn_find_deskRoomOk).setOnClickListener(view1 -> {
            Intent intent = new Intent();
            intent.putIntegerArrayListExtra("amenities", mSelectedAmenities);
            intent.putExtra("layoutId", mSelectedLayout);
            intent.putExtra("campusId", mSelectedCampusId);
            intent.putExtra("buildingId", mSelectedBuildingId);
            intent.putExtra("floorId", mSelectedFloorId);
            intent.putExtra("date", mSelectedDate);
            intent.putExtra("sTime", mSelectedSTime);
            intent.putExtra("eTime", mSelectedETime);
            intent.putExtra("capacity", mSelectedCapacity);
            intent.putExtra("locationType", mSelectedLocationType);

            Constants.SELECTED_BOOK_DATE = mSelectedDate;
            Constants.SELECTED_BOOK_STIME = mSelectedSTime;
            Constants.SELECTED_BOOK_ETIME = mSelectedETime;

            setResult(RESULT_OK, intent);
            finish();
        });

        // Amenities
        img_ameniPhone.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_TELEFON);
        });
        img_ameniSesSistemi.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_SES_SISTEMI);
        });
        img_ameniMonitor.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_MONITOR);

        });
        img_ameniAkilliTahta.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_AKILLI_TAHTA);

        });
        img_ameniKlima.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_KLIMA);

        });
        img_ameniPerde.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_PERDE);

        });
        img_ameniWifi.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_WIFI);

        });
        img_ameniKablosuz.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_KABLOSUZ);

        });
        img_ameniYazi.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_YAZI_TAHTASI);

        });
        img_ameniVideo.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            addAsAmenity(view2, Layouts.AMENITY_VIDEO_KONFERANS);
        });

        //Sitting Plan
        img_sitYuvMasa.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_YUVARLAK_MASA;
            else
                mSelectedLayout = 0;

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);

        });

        img_sitOval.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_OVAL_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitKare.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            mSelectedLayout = Layouts.SITTING_KARE_MASA;

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_KARE_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitUMasa.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);
            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_U_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitDrtgenMasa.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_DIKDORTGEN_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitAmfi.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_AMFI_DUZEN;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitSinif.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_SINIF_DUZEN;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitTekCalisma.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_TEKLI_CALISMA_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitSiraCalisma.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_SIRA_CALISMA_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitKupCalisma.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_KUP_CALISMA_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitUcgenCalisma.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_UCGEN_CALISMA_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitOvalSiraCalisma.setTag(true);
            setBookrenColor(img_sitOvalSiraCalisma, false);
        });

        img_sitOvalSiraCalisma.setOnClickListener(view2 -> {
            setBookrenColor(view2, false);

            if ((boolean) view2.getTag())
                mSelectedLayout = Layouts.SITTING_OVAL_SIRA_CALISMA_MASA;
            else
                mSelectedLayout = 0;

            img_sitYuvMasa.setTag(true);
            setBookrenColor(img_sitYuvMasa, false);

            img_sitOval.setTag(true);
            setBookrenColor(img_sitOval, false);

            img_sitKare.setTag(true);
            setBookrenColor(img_sitKare, false);

            img_sitUMasa.setTag(true);
            setBookrenColor(img_sitUMasa, false);

            img_sitDrtgenMasa.setTag(true);
            setBookrenColor(img_sitDrtgenMasa, false);

            img_sitAmfi.setTag(true);
            setBookrenColor(img_sitAmfi, false);

            img_sitSinif.setTag(true);
            setBookrenColor(img_sitSinif, false);

            img_sitTekCalisma.setTag(true);
            setBookrenColor(img_sitTekCalisma, false);

            img_sitSiraCalisma.setTag(true);
            setBookrenColor(img_sitSiraCalisma, false);

            img_sitKupCalisma.setTag(true);
            setBookrenColor(img_sitKupCalisma, false);

            img_sitUcgenCalisma.setTag(true);
            setBookrenColor(img_sitUcgenCalisma, false);
        });

        //Capacity
        ly_capacity1_4.setOnClickListener(view1 -> {
            setBookrenColor(view1, false);
            mSelectedCapacity = 1;
            ly_capacity5_10.setTag(true);
            setBookrenColor(ly_capacity5_10, false);

            ly_capacity11_20.setTag(true);
            setBookrenColor(ly_capacity11_20, false);

            ly_capacity20.setTag(true);
            setBookrenColor(ly_capacity20, false);
        });

        ly_capacity5_10.setOnClickListener(view1 -> {
            setBookrenColor(view1, false);
            mSelectedCapacity = 5;

            ly_capacity1_4.setTag(true);
            setBookrenColor(ly_capacity1_4, false);

            ly_capacity11_20.setTag(true);
            setBookrenColor(ly_capacity11_20, false);

            ly_capacity20.setTag(true);
            setBookrenColor(ly_capacity20, false);
        });

        ly_capacity11_20.setOnClickListener(view1 -> {
            setBookrenColor(view1, false);
            mSelectedCapacity = 11;

            ly_capacity1_4.setTag(true);
            setBookrenColor(ly_capacity1_4, false);

            ly_capacity5_10.setTag(true);
            setBookrenColor(ly_capacity5_10, false);

            ly_capacity20.setTag(true);
            setBookrenColor(ly_capacity20, false);

        });

        ly_capacity20.setOnClickListener(view1 -> {
            setBookrenColor(view1, false);
            mSelectedCapacity = 20;

            ly_capacity1_4.setTag(true);
            setBookrenColor(ly_capacity1_4, false);

            ly_capacity5_10.setTag(true);
            setBookrenColor(ly_capacity5_10, false);

            ly_capacity11_20.setTag(true);
            setBookrenColor(ly_capacity11_20, false);
        });


        //Location
        tv_locType_customOffice.setOnClickListener(view1 -> {

            if (!fromCustomOfficeHistory) {
                startActivityForResult(new Intent(FindActivity.this, CustomOfficeActivity.class), REQUEST_CUSTOM_OFFICE);
            } else {
                fromCustomOfficeHistory = false;
            }

            setBookrenColor(view1, true);

            tv_locType_myoffice.setTag(true);
            setBookrenColor(tv_locType_myoffice, true);

            tv_locType_nearby.setTag(true);
            setBookrenColor(tv_locType_nearby, true);


        });

        tv_locType_myoffice.setOnClickListener(view1 -> {
            setBookrenColor(view1, true);

            tv_locType_customOffice.setTag(true);
            setBookrenColor(tv_locType_customOffice, true);

            tv_locType_nearby.setTag(true);
            setBookrenColor(tv_locType_nearby, true);


            mSelectedCampusId = MyPreferences.GetCampusPref(FindActivity.this);
            mSelectedBuildingId = MyPreferences.GetBuildingPref(FindActivity.this);
            mSelectedFloorId = MyPreferences.GetFloorPref(FindActivity.this);
        });

        tv_locType_nearby.setOnClickListener(view1 -> {

            setBookrenColor(view1, true);
            tv_locType_myoffice.setTag(true);
            setBookrenColor(tv_locType_myoffice, true);

            tv_locType_customOffice.setTag(true);
            setBookrenColor(tv_locType_customOffice, true);
        });

        //Availability
        tv_availableOther.setOnClickListener(view2 -> {
            if (!fromAvailOtherHistory) {
                startActivityForResult(new Intent(FindActivity.this, FindDateSelectActivity.class), REQUEST_CUSTOM_DATE);
            } else {
                fromAvailOtherHistory = false;
            }

            setBookrenColor(view2, true);

            tv_available2Hr.setTag(true);
            setBookrenColor(tv_available2Hr, true);

            tv_available1Hr.setTag(true);
            setBookrenColor(tv_available1Hr, true);

            tv_available30.setTag(true);
            setBookrenColor(tv_available30, true);

            tv_available15.setTag(true);
            setBookrenColor(tv_available15, true);

        });

        tv_available2Hr.setOnClickListener(view2 -> {
            setBookrenColor(view2, true);
            mSelectedDate = DateUtil.getCurrentDate();
            mSelectedSTime = DateUtil.getCurrentTime(0);
            mSelectedETime = DateUtil.getCurrentTime(120);

            Constants.SELECTED_BOOK_MIN = 120;

            tv_availableOther.setTag(true);
            setBookrenColor(tv_availableOther, true);

            tv_available1Hr.setTag(true);
            setBookrenColor(tv_available1Hr, true);

            tv_available30.setTag(true);
            setBookrenColor(tv_available30, true);

            tv_available15.setTag(true);
            setBookrenColor(tv_available15, true);

        });

        tv_available1Hr.setOnClickListener(view2 -> {
            setBookrenColor(view2, true);
            mSelectedDate = DateUtil.getCurrentDate();
            mSelectedSTime = DateUtil.getCurrentTime(0);
            mSelectedETime = DateUtil.getCurrentTime(60);
            tv_availableOther.setTag(true);
            setBookrenColor(tv_availableOther, true);
            Constants.SELECTED_BOOK_MIN = 60;

            tv_available2Hr.setTag(true);
            setBookrenColor(tv_available2Hr, true);

            tv_available30.setTag(true);
            setBookrenColor(tv_available30, true);

            tv_available15.setTag(true);
            setBookrenColor(tv_available15, true);
        });

        tv_available30.setOnClickListener(view2 -> {
            setBookrenColor(view2, true);
            mSelectedDate = DateUtil.getCurrentDate();
            mSelectedSTime = DateUtil.getCurrentTime(0);
            mSelectedETime = DateUtil.getCurrentTime(30);
            tv_availableOther.setTag(true);
            setBookrenColor(tv_availableOther, true);
            Constants.SELECTED_BOOK_MIN = 30;

            tv_available2Hr.setTag(true);
            setBookrenColor(tv_available2Hr, true);

            tv_available1Hr.setTag(true);
            setBookrenColor(tv_available1Hr, true);

            tv_available15.setTag(true);
            setBookrenColor(tv_available15, true);
        });

        tv_available15.setOnClickListener(view2 -> {
            setBookrenColor(view2, true);
            mSelectedDate = DateUtil.getCurrentDate();
            mSelectedSTime = DateUtil.getCurrentTime(0);
            mSelectedETime = DateUtil.getCurrentTime(15);
            tv_availableOther.setTag(true);
            setBookrenColor(tv_availableOther, true);
            Constants.SELECTED_BOOK_MIN = 15;

            tv_available2Hr.setTag(true);
            setBookrenColor(tv_available2Hr, true);

            tv_available1Hr.setTag(true);
            setBookrenColor(tv_available1Hr, true);

            tv_available30.setTag(true);
            setBookrenColor(tv_available30, true);
        });

        //RoomDesk
        btn_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() == null || !((boolean) view.getTag())) {
                    view.setTag(true);
                    ((ImageView) view).setImageResource(R.drawable.roomssecim);
                    ((ImageView) findViewById(R.id.img_find_desk)).setImageResource(R.drawable.desk);
                    mSelectedLocationType = MAHAL_TYPE_MEETING;
                    findViewById(R.id.horScroll_amenities).setVisibility(View.VISIBLE);
                    ly_capacity1_4.setVisibility(View.VISIBLE);
                    ly_capacity5_10.setVisibility(View.VISIBLE);
                    ly_capacity11_20.setVisibility(View.VISIBLE);
                    ly_capacity20.setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_find_amenityTitle).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_find_capacityTitle).setVisibility(View.VISIBLE);
                    findViewById(R.id.guide_find_capacityAmeny).setVisibility(View.VISIBLE);

                    img_sitYuvMasa.setVisibility(View.VISIBLE);
                    img_sitOval.setVisibility(View.VISIBLE);
                    img_sitKare.setVisibility(View.VISIBLE);
                    img_sitUMasa.setVisibility(View.VISIBLE);
                    img_sitDrtgenMasa.setVisibility(View.VISIBLE);
                    img_sitAmfi.setVisibility(View.VISIBLE);
                    img_sitSinif.setVisibility(View.VISIBLE);

                    img_sitTekCalisma.setVisibility(View.GONE);
                    img_sitSiraCalisma.setVisibility(View.GONE);
                    img_sitKupCalisma.setVisibility(View.GONE);
                    img_sitUcgenCalisma.setVisibility(View.GONE);
                    img_sitOvalSiraCalisma.setVisibility(View.GONE);

                    ((Guideline) findViewById(R.id.guide_find_amentySitting)).setGuidelinePercent(0.76f);


                } else {
                    view.setTag(false);
                    ((ImageView) view).setImageResource(R.drawable.rooms);

                }
            }
        });

        btnDesk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() == null || !((boolean) view.getTag())) {
                    view.setTag(true);
                    ((ImageView) view).setImageResource(R.drawable.desksecim);
                    ((ImageView) findViewById(R.id.img_find_room)).setImageResource(R.drawable.rooms);
                    mSelectedLocationType = MAHAL_TYPE_DESK;
                    findViewById(R.id.horScroll_amenities).setVisibility(View.GONE);
                    ly_capacity1_4.setVisibility(View.GONE);
                    ly_capacity5_10.setVisibility(View.GONE);
                    ly_capacity11_20.setVisibility(View.GONE);
                    ly_capacity20.setVisibility(View.GONE);
                    findViewById(R.id.tv_find_amenityTitle).setVisibility(View.GONE);
                    findViewById(R.id.tv_find_capacityTitle).setVisibility(View.GONE);
                    findViewById(R.id.guide_find_capacityAmeny).setVisibility(View.GONE);

                    //  img_sitYuvMasa.setVisibility(View.GONE);
                    img_sitOval.setVisibility(View.GONE);
                    // img_sitKare.setVisibility(View.GONE);
                    img_sitUMasa.setVisibility(View.GONE);
                    img_sitDrtgenMasa.setVisibility(View.GONE);
                    img_sitAmfi.setVisibility(View.GONE);
                    img_sitSinif.setVisibility(View.GONE);

                    img_sitTekCalisma.setVisibility(View.VISIBLE);
                    img_sitSiraCalisma.setVisibility(View.VISIBLE);
                    img_sitKupCalisma.setVisibility(View.VISIBLE);
                    img_sitUcgenCalisma.setVisibility(View.VISIBLE);
                    img_sitOvalSiraCalisma.setVisibility(View.VISIBLE);

                    ((Guideline) findViewById(R.id.guide_find_amentySitting)).setGuidelinePercent(0.48f);

                } else {
                    view.setTag(false);
                    ((ImageView) view).setImageResource(R.drawable.desk);

                }
            }
        });

        getSearchBookingHistory();

    }

    private void getSearchBookingHistory() {
        try {
            mSearchBooking = (SearchBooking) MyPreferences.getJsonObject(FindActivity.this, "mySearchBooking");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mSearchBooking != null) {
            if (mSearchBooking.getLocationType() == MAHAL_TYPE_DESK)
                btnDesk.performClick();
            else
                btn_room.performClick();

            if (mSearchBooking.getCapacity() != 0) {
                switch (mSearchBooking.getCapacity()) {
                    case 1:
                        ly_capacity1_4.performClick();
                        break;
                    case 5:
                        ly_capacity5_10.performClick();
                        break;
                    case 11:
                        ly_capacity11_20.performClick();
                        break;
                    case 20:
                        ly_capacity20.performClick();
                        break;
                }
            }

            switch (Constants.SELECTED_BOOK_MIN) {
                case 15:
                    tv_available15.performClick();
                    break;
                case 30:
                    tv_available30.performClick();
                    break;
                case 60:
                    tv_available1Hr.performClick();
                    break;
                case 120:
                    tv_available2Hr.performClick();
                    break;
                case 0:
                    break;
                default:
                    fromAvailOtherHistory = true;
                    tv_availableOther.performClick();
            }

            if (mSearchBooking.getOffice() != null) {
                Office office = mSearchBooking.getOffice();
                int campusId = MyPreferences.GetCampusPref(this);
                int buildingId = MyPreferences.GetBuildingPref(this);
                int floorId = MyPreferences.GetFloorPref(this);
                if (campusId == office.getCampusID() && buildingId == office.getBuildingID() && floorId == office.getFloorID()) {
                    tv_locType_myoffice.performClick();
                } else {
                    fromCustomOfficeHistory = true;
                    tv_locType_customOffice.performClick();

                }
            }
            if (mSearchBooking.getLocationType() == MAHAL_TYPE_MEETING) {
                if (!mSearchBooking.getEquipmentIds().isEmpty()) {
                    for (Integer equipmentId : mSearchBooking.getEquipmentIds()) {
                        switch (equipmentId) {
                            case Layouts.AMENITY_TELEFON:
                                img_ameniPhone.performClick();
                                break;
                            case Layouts.AMENITY_SES_SISTEMI:
                                img_ameniSesSistemi.performClick();
                                break;
                            case Layouts.AMENITY_MONITOR:
                                img_ameniMonitor.performClick();
                                break;
                            case Layouts.AMENITY_AKILLI_TAHTA:
                                img_ameniAkilliTahta.performClick();
                                break;
                            case Layouts.AMENITY_KLIMA:
                                img_ameniKlima.performClick();
                                break;
                            case Layouts.AMENITY_PERDE:
                                img_ameniPerde.performClick();
                                break;
                            case Layouts.AMENITY_WIFI:
                                img_ameniWifi.performClick();
                                break;
                            case Layouts.AMENITY_KABLOSUZ:
                                img_ameniKablosuz.performClick();
                                break;
                            case Layouts.AMENITY_YAZI_TAHTASI:
                                img_ameniYazi.performClick();
                                break;
                            case Layouts.AMENITY_VIDEO_KONFERANS:
                                img_ameniVideo.performClick();
                                break;
                        }
                    }
                }
            }
            if (mSearchBooking.getLayoutId() != 0) {
                switch (mSearchBooking.getLayoutId()) {
                    case Layouts.SITTING_YUVARLAK_MASA:
                        img_sitYuvMasa.performClick();
                        break;
                    case Layouts.SITTING_OVAL_MASA:
                        img_sitOval.performClick();
                        break;
                    case Layouts.SITTING_KARE_MASA:
                        img_sitKare.performClick();
                        break;
                    case Layouts.SITTING_U_MASA:
                        img_sitUMasa.performClick();
                        break;
                    case Layouts.SITTING_DIKDORTGEN_MASA:
                        img_sitDrtgenMasa.performClick();
                        break;
                    case Layouts.SITTING_AMFI_DUZEN:
                        img_sitAmfi.performClick();
                        break;
                    case Layouts.SITTING_SINIF_DUZEN:
                        img_sitSinif.performClick();
                        break;
                    case Layouts.SITTING_TEKLI_CALISMA_MASA:
                        img_sitTekCalisma.performClick();
                        break;
                    case Layouts.SITTING_SIRA_CALISMA_MASA:
                        img_sitSiraCalisma.performClick();
                        break;
                    case Layouts.SITTING_KUP_CALISMA_MASA:
                        img_sitKupCalisma.performClick();
                        break;
                    case Layouts.SITTING_UCGEN_CALISMA_MASA:
                        img_sitUcgenCalisma.performClick();
                        break;
                    case Layouts.SITTING_OVAL_SIRA_CALISMA_MASA:
                        img_sitOvalSiraCalisma.performClick();
                        break;
                }
            }

        }
    }

    private void setviewResources() {

        btn_room = findViewById(R.id.img_find_room);
        btnDesk = findViewById(R.id.img_find_desk);

        tv_available15 = findViewById(R.id.tv_find_avail15min);
        tv_available30 = findViewById(R.id.tv_find_avail30min);
        tv_available1Hr = findViewById(R.id.tv_find_avail1hr);
        tv_available2Hr = findViewById(R.id.tv_find_avail2hr);
        tv_availableOther = findViewById(R.id.tv_find_availOther);

        tv_locType_nearby = findViewById(R.id.tv_location_nearby);
        tv_locType_myoffice = findViewById(R.id.tv_location_myOffice);
        tv_locType_customOffice = findViewById(R.id.tv_location_custom);

        ly_capacity1_4 = findViewById(R.id.ly_capacity_1_4);
        ly_capacity5_10 = findViewById(R.id.ly_capacity_5_10);
        ly_capacity11_20 = findViewById(R.id.ly_capacity_11_20);
        ly_capacity20 = findViewById(R.id.ly_capacity_20);

        img_ameniPhone = findViewById(R.id.img_ameni_phone);
        img_ameniSesSistemi = findViewById(R.id.img_ameni_sesSistemi);
        img_ameniMonitor = findViewById(R.id.img_ameni_monitor);
        img_ameniAkilliTahta = findViewById(R.id.img_ameni_akilliTahta);
        img_ameniKlima = findViewById(R.id.img_ameni_klima);
        img_ameniPerde = findViewById(R.id.img_ameni_perde);
        img_ameniWifi = findViewById(R.id.img_ameni_wifi);
        img_ameniKablosuz = findViewById(R.id.img_ameni_kablosuz);
        img_ameniYazi = findViewById(R.id.img_ameni_yazi);
        img_ameniVideo = findViewById(R.id.img_ameni_video);

        img_sitYuvMasa = findViewById(R.id.img_sitting_yuvarlakMasa);
        img_sitOval = findViewById(R.id.img_sitting_oval);
        img_sitKare = findViewById(R.id.img_sitting_kareMasa);
        img_sitUMasa = findViewById(R.id.img_sitting_uMasa);
        img_sitDrtgenMasa = findViewById(R.id.img_sitting_dikdortgenMasa);
        img_sitAmfi = findViewById(R.id.img_sitting_amfiDuzeni);
        img_sitSinif = findViewById(R.id.img_sitting_sinifDuzeni);
        img_sitTekCalisma = findViewById(R.id.img_sittingDesk_tekliCalismaMasa);
        img_sitSiraCalisma = findViewById(R.id.img_sittingDesk_siraCalismaMasa);
        img_sitKupCalisma = findViewById(R.id.img_sittingDesk_kupCalismaMasa);
        img_sitUcgenCalisma = findViewById(R.id.img_sittingDesk_ucgenCalismaMasa);
        img_sitOvalSiraCalisma = findViewById(R.id.img_sittingDesk_ovalSiraCalismaMasa);
    }

    private void setBookrenColor(View view, boolean isBordered) {
        if (view.getTag() == null || !((boolean) view.getTag())) {
            view.setBackgroundColor(getResources().getColor(R.color.colorBookreen));
            view.setTag(true);
        } else {
            view.setBackgroundColor(Color.WHITE);
            if (isBordered)
                view.setBackground(getResources().getDrawable(R.drawable.text_border));
            view.setTag(false);
        }
    }

    private void addAsAmenity(View view, Integer amenityId) {
        if (((boolean) view.getTag())) {
            mSelectedAmenities.add(amenityId);
        } else {
            mSelectedAmenities.remove(amenityId);

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CUSTOM_OFFICE) {
            if (resultCode == RESULT_OK) {
                mSelectedCampusId = data.getIntExtra("campusId", 0);
                mSelectedBuildingId = data.getIntExtra("buildingId", 0);
                mSelectedFloorId = data.getIntExtra("floorId", 0);

                tv_locType_myoffice.setTag(false);
                setBookrenColor(tv_locType_myoffice, true);

                tv_locType_myoffice.setTag(true);
                setBookrenColor(tv_locType_myoffice, true);

                tv_locType_nearby.setTag(true);
                setBookrenColor(tv_locType_nearby, true);

                fromCustomOfficeHistory = true;
                tv_locType_customOffice.setTag(false);
                tv_locType_customOffice.performClick();

            } else {
                tv_locType_myoffice.performClick();
            }

        }
        if (requestCode == REQUEST_CUSTOM_DATE) {
            if (resultCode == RESULT_OK) {
                mSelectedDate = data.getStringExtra("selectedDate");
                mSelectedSTime = data.getStringExtra("selectedSTime");
                mSelectedETime = data.getStringExtra("selectedEDate");

                Constants.SELECTED_BOOK_DATE = mSelectedDate;
                Constants.SELECTED_BOOK_STIME = mSelectedSTime;
                Constants.SELECTED_BOOK_ETIME = mSelectedETime;

                fromAvailOtherHistory = true;
                tv_availableOther.setTag(false);
                tv_availableOther.performClick();
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}