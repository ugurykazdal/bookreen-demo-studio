package com.unisoft.bookreen1.ui.adapter;

import com.unisoft.bookreen1.R;

public class Layouts {
    public static final int AMENITY_MONITOR = 126;
    public static final int AMENITY_SES_SISTEMI = 127;
    public static final int AMENITY_TELEFON = 128;
    public static final int AMENITY_AKILLI_TAHTA = 129;
    public static final int AMENITY_VIDEO_KONFERANS = 130;
    public static final int AMENITY_YAZI_TAHTASI = 131;
    public static final int AMENITY_PERDE = 132;
    public static final int AMENITY_WIFI = 133;
    public static final int AMENITY_KLIMA = 134;
    public static final int AMENITY_KABLOSUZ = 135;

    public static final int SITTING_YUVARLAK_MASA = 2074;
    public static final int SITTING_OVAL_MASA = 2075;
    public static final int SITTING_KARE_MASA = 2076;
    public static final int SITTING_U_MASA = 2077;
    public static final int SITTING_DIKDORTGEN_MASA = 2078;
    public static final int SITTING_AMFI_DUZEN = 2079;
    public static final int SITTING_SINIF_DUZEN = 2080;
    public static final int SITTING_TEKLI_CALISMA_MASA = 2110;
    public static final int SITTING_SIRA_CALISMA_MASA = 2111;
    public static final int SITTING_KUP_CALISMA_MASA = 2112;
    public static final int SITTING_UCGEN_CALISMA_MASA = 2113;
    public static final int SITTING_OVAL_SIRA_CALISMA_MASA = 3111;

    public static int getLayoutDrawable(int layoutId) {
        switch (layoutId) {
            case SITTING_YUVARLAK_MASA:
                return R.drawable.yuvarlak_masa;
            case SITTING_OVAL_MASA:
                return R.drawable.oval_masa;
            case SITTING_KARE_MASA:
                return R.drawable.kare_masa;
            case SITTING_U_MASA:
                return R.drawable.u_masa;
            case SITTING_DIKDORTGEN_MASA:
                return R.drawable.dikdortgen_masa;
            case SITTING_AMFI_DUZEN:
                return R.drawable.amfi_duzeni;
            case SITTING_SINIF_DUZEN:
                return R.drawable.sinif_duzeni;
            case SITTING_TEKLI_CALISMA_MASA:
                return R.drawable.tekli_calisma_masasi;
            case SITTING_SIRA_CALISMA_MASA:
                return R.drawable.sira_calisma_masasi;
            case SITTING_KUP_CALISMA_MASA:
                return R.drawable.kup_calisma_masasi;
            case SITTING_UCGEN_CALISMA_MASA:
                return R.drawable.ucgen_calisma_masasi;
            case SITTING_OVAL_SIRA_CALISMA_MASA:
                return R.drawable.oval_sira_calisma_masasi;
            default:
                return R.drawable.kare_masa;
        }
    }

}
