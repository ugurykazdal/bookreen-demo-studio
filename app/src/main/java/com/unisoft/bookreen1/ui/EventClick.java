package com.unisoft.bookreen1.ui;


import com.google.api.services.calendar.model.Event;

public interface EventClick {
    void eventClick(Event event);
}

