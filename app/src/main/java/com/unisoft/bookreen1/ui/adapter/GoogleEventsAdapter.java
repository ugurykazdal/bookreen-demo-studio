package com.unisoft.bookreen1.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.ui.EventClick;
import com.unisoft.bookreen1.util.DateUtil;
import com.unisoft.bookreen1.util.GoogleCalendarUtil;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

public class GoogleEventsAdapter extends RecyclerView.Adapter<GoogleEventsAdapter.MyViewHolder> {
    private List<Event> mDataset;
    View v;
    EventClick myListener;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tv_calendarDay;
        TextView tv_eventDuration;
        TextView tv_eventHour;
        TextView tv_eventName;
        TextView tv_eventMahal;

        MyViewHolder(View v) {
            super(v);
            tv_calendarDay = v.findViewById(R.id.tv_itemCalendar_day);
            tv_eventDuration = v.findViewById(R.id.tv_itemCalendar_eventDuration);
            tv_eventHour = v.findViewById(R.id.tv_itemCalendar_eventHour);
            tv_eventName = v.findViewById(R.id.tv_itemCalendar_eventName);
            tv_eventMahal = v.findViewById(R.id.tv_itemCalendar_eventMahal);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public GoogleEventsAdapter(List<Event> myDataset,EventClick listener) {
        mDataset = myDataset;
        myListener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GoogleEventsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_calendar, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Event event = mDataset.get(position);

        DateTime start = new DateTime(event.getStart().getDateTime().toString());
        if (start != null) {
            try {
                holder.tv_calendarDay.setText(DateUtil.formatGoogleCalendarDate(start.toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        holder.tv_eventName.setText(event.getSummary());

        if (event.getStart().getDateTime()==null)
            holder.tv_eventHour.setText("00:00");
        else
            holder.tv_eventHour.setText(event.getStart().getDateTime().toString().substring(11,16)+" - "+event.getEnd().getDateTime().toString().substring(11,16));

       // holder.tv_eventDuration.setText(mDataset.get(position).getStart().toString());
        holder.tv_eventDuration.setText(event.getOrganizer().getEmail());
        holder.tv_eventMahal.setText(event.getLocation());
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myListener.eventClick(event);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



}