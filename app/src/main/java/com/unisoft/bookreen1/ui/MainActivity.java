package com.unisoft.bookreen1.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.unisoft.bookreen1.util.Constants;
import com.unisoft.bookreen1.R;

public class MainActivity extends AppCompatActivity {
    public BottomNavigationView bottomNavigationView;
    public HomeFragment homeFragment;
    public CalendarFragment calendarFragment;
    public OccasionFragment occasionFragment;
    public PinFragment pinFragment;
    public Fragment mSelectedFragment = null;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        homeFragment = new HomeFragment();
        calendarFragment = new CalendarFragment();
        occasionFragment = new OccasionFragment();
        pinFragment = new PinFragment();


        if (getIntent().getExtras() != null) {
            String className = getIntent().getExtras().getString("fragment", homeFragment.getClass().getSimpleName());
            if (className.equals(homeFragment.getClass().getSimpleName())) {
                loadFragment(homeFragment);
            } else if (className.equals(calendarFragment.getClass().getSimpleName())) {
                loadFragment(calendarFragment);
            } else if (className.equals(occasionFragment.getClass().getSimpleName())) {
                loadFragment(occasionFragment);
            } else {
                loadFragment(pinFragment);
            }
        } else {
            loadFragment(homeFragment);
        }

        bottomNavigationView = findViewById(R.id.nav_view);
        BottomNavigationView.OnNavigationItemSelectedListener listener = item -> {
            switch (item.getItemId()) {
                case R.id.nav_events:
                    mSelectedFragment = homeFragment;
                    break;
                case R.id.navigation_calendar:
                    mSelectedFragment = calendarFragment;
                    break;
                case R.id.navigation_add:
                    if (!homeFragment.isAnyMinuteSelected()) {
                        Constants.SELECTED_BOOK_MIN = 30;
                    }
                    mSelectedFragment = occasionFragment;
                    break;
                case R.id.navigation_pin:
                    mSelectedFragment = pinFragment;
                    break;
            }
            loadFragment(mSelectedFragment);

            return true;
        };
        bottomNavigationView.setOnNavigationItemSelectedListener(listener);

        Constants.mainActivity = this;
    }

    public boolean loadFragment(Fragment fragment) {
        mSelectedFragment = fragment;
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mSelectedFragment instanceof PinFragment) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_add);
        } else if (!(mSelectedFragment instanceof HomeFragment)) {
            bottomNavigationView.setSelectedItemId(R.id.nav_events);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
    }


    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Intent intent = getIntent();
        intent.putExtra("fragment", mSelectedFragment.getClass().getSimpleName());
        finish();
        startActivity(intent);
    }

}