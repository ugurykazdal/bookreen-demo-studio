package com.unisoft.bookreen1.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.util.MyPreferences;

import java.io.IOException;

public class LinkCalendarActivity extends AppCompatActivity implements View.OnClickListener {
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 20;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.actvitiy_link);

        findViewById(R.id.img_linkCalendar_google).setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }


    private void updateUI(GoogleSignInAccount account) {
        if (account != null)
            getAccessToken();
    }

    private void returnActivity(String token) {
        if (token != null)
            MyPreferences.SaveGoogleCalendarTokenPref(LinkCalendarActivity.this, token);

        Intent intent = new Intent();
        intent.putExtra("token", token);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        if (requestCode == 22) {
            if (resultCode == RESULT_OK) {
                getAccessToken();
            } else {
                mGoogleSignInClient.signOut();
            }
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("GOOGLE SIGNIN", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_linkCalendar_google:
                signIn();
                break;
        }
    }

    private void getAccessToken() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        if (account != null) {
            Log.i("TEL TEL", getLocalClassName());
            try {
                AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        String token = null;
                        final String SCOPES = "https://www.googleapis.com/auth/calendar.events";

                        try {
                            token = GoogleAuthUtil.getToken(
                                    getApplicationContext(), account.getAccount().name
                                    , "oauth2:" + SCOPES);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (UserRecoverableAuthException e) {
                            startActivityForResult(e.getIntent(), 22);
                            e.printStackTrace();
                            return null;
                        } catch (GoogleAuthException e) {
                            e.printStackTrace();
                        }

                        return token;

                    }

                    @Override
                    protected void onPostExecute(String token) {
                        try {
                            if (token != null)
                                returnActivity(token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", "Access token retrieved:" + token);
                    }

                };
                task.execute();
                // String token = GoogleAuthUtil.getToken(LoginActivity.this, account.getAccount().name, "https://www.googleapis.com/auth/calendar.events");
                //Log.v("token",token);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //      startActivity(new Intent(LoginActivity.this, MainActivity.class));

        }
    }

}
