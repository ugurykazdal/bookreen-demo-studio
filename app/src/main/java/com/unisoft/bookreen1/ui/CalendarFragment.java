package com.unisoft.bookreen1.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.api.services.calendar.model.Event;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.ui.adapter.GoogleEventsAdapter;
import com.unisoft.bookreen1.util.GoogleCalendarUtil;
import com.unisoft.bookreen1.util.MyPreferences;

import java.io.IOException;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CalendarFragment extends Fragment implements EventClick{

    private static final int REQUEST_LINK_CALENDAR = 123;
    private ImageView img_linkCalendar;
    private RecyclerView recGoogleCalendar;
    private GoogleEventsAdapter mAdapter;
    private ProgressBar pbCalendar;
    private LinearLayout mCalendarNoContent;
    private SignInButton mSignOutButton;
    private TextView tv_accountName;
    private FloatingActionButton fabAddEvent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        img_linkCalendar = view.findViewById(R.id.img_calendar_link);
        recGoogleCalendar = view.findViewById(R.id.rec_calendar_googleEvents);
        recGoogleCalendar.setHasFixedSize(true);
        recGoogleCalendar.setLayoutManager(new LinearLayoutManager(getContext()));
        pbCalendar = view.findViewById(R.id.pb_calendar);
        pbCalendar.setVisibility(View.VISIBLE);
        mCalendarNoContent = view.findViewById(R.id.ly_nocontenCalendar);

        mSignOutButton = view.findViewById(R.id.sign_out_button);
        mSignOutButton.setSize(SignInButton.SIZE_STANDARD);
        tv_accountName = view.findViewById(R.id.tv_calendar_accountMail);

        fabAddEvent = view.findViewById(R.id.fab_addEvent);
        fabAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), CreateNewGoogleEvent.class));

            }
        });

        ((TextView) mSignOutButton.getChildAt(0)).setText("Sign Out");
        mSignOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doGoogleSignOut();
            }
        });

        img_linkCalendar.setOnClickListener(view1 -> getActivity().startActivityForResult(new Intent(getActivity(), LinkCalendarActivity.class), REQUEST_LINK_CALENDAR));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LINK_CALENDAR) {
            if (resultCode == RESULT_OK) {
                String token = data.getStringExtra("token");
                getEvents(token);
            }
        }
    }

    public void getEvents(String token) {
        new GoogleCalendarUtil(token, CalendarFragment.this, getActivity()).getEvents();
    }

    public void doYourGoogleEventsWorks(List<Event> _events) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pbCalendar.setVisibility(View.GONE);
                if (_events.isEmpty()) {
                    recGoogleCalendar.setVisibility(View.GONE);
                    mCalendarNoContent.setVisibility(View.VISIBLE);
                    img_linkCalendar.setVisibility(View.GONE);
                    return;
                }
                recGoogleCalendar.setVisibility(View.VISIBLE);
                img_linkCalendar.setVisibility(View.GONE);
                mCalendarNoContent.setVisibility(View.GONE);

                GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());

                tv_accountName.setText(account.getEmail());
                mSignOutButton.setVisibility(View.VISIBLE);
                tv_accountName.setVisibility(View.VISIBLE);

                setRecycleAdapter(_events);
            }
        });

    }

    private void setRecycleAdapter(List<Event> events) {
        mAdapter = new GoogleEventsAdapter(events,CalendarFragment.this::eventClick);
        mAdapter.setHasStableIds(true);
        recGoogleCalendar.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());
        if (account != null) {
            String token = MyPreferences.GetGoogleCalendarTokenPref(getContext());
            getEvents(token);
            tv_accountName.setText(account.getEmail());
            mSignOutButton.setVisibility(View.VISIBLE);
            tv_accountName.setVisibility(View.VISIBLE);
        } else {
            mSignOutButton.setVisibility(View.GONE);
            tv_accountName.setVisibility(View.GONE);
            pbCalendar.setVisibility(View.GONE);
            img_linkCalendar.setVisibility(View.VISIBLE);
        }
        super.onStart();

    }

    public void doGoogleSignOut() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleSignInClient signInClient = GoogleSignIn.getClient(getActivity(), gso);
        signInClient.signOut();
        pbCalendar.setVisibility(View.GONE);
        img_linkCalendar.setVisibility(View.VISIBLE);
        recGoogleCalendar.setVisibility(View.GONE);
        mSignOutButton.setVisibility(View.GONE);
        tv_accountName.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceType")
    @Override
    public void eventClick(Event event) {
        PopupMenu popup = new PopupMenu(getContext(),getView(), Gravity.NO_GRAVITY,R.attr.actionOverflowMenuStyle, 0);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.layout.eventmenu, popup.getMenu());


        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    GoogleCalendarUtil.service.events().delete("primary", event.getId()).execute();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                        Toast.makeText(getContext(),item.getTitle()+" Gerçekleşti ", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.update:
                        Intent updateIntent=new Intent(getContext(),UpdateEvent.class);
                        updateIntent.putExtra("eventId",event.getId());
                       startActivity(updateIntent);
                        return true;
                    default:
                        return false;
                }

            }
        });

        popup.show();//showing popup menu



    }
}