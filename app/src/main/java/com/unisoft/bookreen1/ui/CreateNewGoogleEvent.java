package com.unisoft.bookreen1.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.util.GoogleCalendarUtil;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.TimeZone;

public class CreateNewGoogleEvent extends AppCompatActivity {

    private String mTimeZone;
    private TextInputLayout mSubject;
    private TextInputLayout mAttendees;
    private TextInputLayout mStartInputLayout;
    private TextInputLayout mEndInputLayout;
    private TextInputLayout mBody;
    private EditTextDateTimePicker mStartPicker;
    private EditTextDateTimePicker mEndPicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_google_event);

        mTimeZone = "Europe/Istanbul";
        ZoneId userTimeZone = GraphToIana.getZoneIdFromWindows(mTimeZone);

        mSubject = findViewById(R.id.neweventsubject);
        mAttendees = findViewById(R.id.neweventattendees);
        mBody = findViewById(R.id.neweventbody);

        mStartInputLayout = findViewById(R.id.neweventstartdatetime);
        mStartPicker = new EditTextDateTimePicker(CreateNewGoogleEvent.this,
                mStartInputLayout.getEditText(),
                userTimeZone);

        mEndInputLayout = findViewById(R.id.neweventenddatetime);
        mEndPicker = new EditTextDateTimePicker(CreateNewGoogleEvent.this,
                mEndInputLayout.getEditText(),
                userTimeZone);


        findViewById(R.id.createevent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String subject = mSubject.getEditText().getText().toString();
                            String attendees = mAttendees.getEditText().getText().toString();
                            String body = mBody.getEditText().getText().toString();
                            String[] attendeeArray = attendees.split(";");
                            ZonedDateTime startDateTime = mStartPicker.getZonedDateTime();
                            String startDateTime1 = startDateTime.toString().substring(0, 16);
                            ZonedDateTime endDateTime = mEndPicker.getZonedDateTime();
                            String endDateTime1 = endDateTime.toString().substring(0, 16);
                            createEvent(subject, startDateTime1, endDateTime1, body);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });


    }

    private void createEvent(String subject,
                             String startDateTime1,
                             String endDateTime1,
                             String body) {

        Event event = new Event()
                .setSummary(subject)
                .setLocation("Bilinmiyor")
                .setDescription(body);

        DateTime startDateTime = new DateTime(startDateTime1+":00-03:00");
        EventDateTime start = new EventDateTime()
                .setDateTime(startDateTime)
                .setTimeZone("Europe/Istanbul");
        event.setStart(start);

        DateTime endDateTime = new DateTime(endDateTime1+":00-03:00");
        EventDateTime end = new EventDateTime()
                .setDateTime(endDateTime)
                .setTimeZone("Europe/Istanbul");
        event.setEnd(end);


        EventAttendee[] attendees = new EventAttendee[]{
                new EventAttendee().setEmail("lpage@example.com"),
                new EventAttendee().setEmail("sbrin@example.com"),
        };
        event.setAttendees(Arrays.asList(attendees));

        EventReminder[] reminderOverrides = new EventReminder[]{
                new EventReminder().setMethod("email").setMinutes(24 * 60),
                new EventReminder().setMethod("popup").setMinutes(10),
        };
        Event.Reminders reminders = new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(reminderOverrides));
        event.setReminders(reminders);

        String calendarId = "primary";
        try {
            event = GoogleCalendarUtil.service.events().insert(calendarId, event).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}