package com.unisoft.bookreen1.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Floor;
import com.unisoft.bookreen1.network.Repository;
import com.unisoft.bookreen1.util.Constants;
import com.unisoft.bookreen1.util.MyPreferences;

import java.util.List;

import static android.app.Activity.RESULT_OK;
import static br.com.zbra.androidlinq.Linq.stream;
import static com.unisoft.bookreen1.util.Constants.SELECTED_BOOK_MIN;
import static com.unisoft.bookreen1.util.Constants.cookies;

public class PinFragment extends Fragment {
    private static final int REQUEST_CUSTOM_OFFICE = 100;
    private WebView myWebView;
    public static String URL;
    private Spinner spinnerFloors;
    private Repository mRepository;
    private int selectedFloor = 9999;
    private List<Floor> mFloorList;
    private FloatingActionButton fabChange;

    private CookieManager cookieManager;
    CookieSyncManager cookieSyncManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pin, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRepository = new Repository(getContext(), PinFragment.this);
        myWebView = (WebView) view.findViewById(R.id.webView);

        fabChange = view.findViewById(R.id.fab_changeCampus);

        fabChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getContext(), CustomOfficeActivity.class), REQUEST_CUSTOM_OFFICE);

            }
        });

        WebSettings webSettings = myWebView.getSettings();

        webSettings.setJavaScriptEnabled(true);
        //**enabled dom storage**
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setDatabaseEnabled(false);
        webSettings.setAppCacheEnabled(false);
        webSettings.setLoadsImagesAutomatically(true);
        cookieSyncManager = CookieSyncManager.createInstance(myWebView.getContext());
        cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.removeSessionCookie();

        String cookieee = "";
        if (cookies != null) {
            for (String cookie : cookies) {
                // Use this cookie whereever you wanna use.
                cookieee = cookie;
                Log.d("Cookie", "PHPSESSID=k7rui2utifgoa50enr1j0parv5;");
            }
        }
        cookieManager.setCookie("http://85.105.163.29", cookieee);
        //setwebcclient
        //  myWebView.loadUrl("http://192.168.1.14/admin");
        //  myWebView.loadUrl("http://85.105.163.29/backend/plugins/mapplic/main/1apartment.php?floorId=1009");
        // "http://85.105.163.29/backend/plugins/mapplic/main/1apartment.php?floorId=2053"
        int buildingId = 0;
        if (Constants.IS_MY_OFFICE) {
            selectedFloor = MyPreferences.GetMyOffice(getContext()).getFloorID();
            buildingId = MyPreferences.GetMyOffice(getContext()).getBuildingID();
        } else {
            selectedFloor = MyPreferences.GetCustomOffice(getContext()).getFloorID();
            buildingId = MyPreferences.GetCustomOffice(getContext()).getBuildingID();
        }



        if (!Constants.FROM_OCCASION_LIST) {
            URL = "http://85.105.163.29/backend/plugins/mapplic/main/2apartment.php?floorId=" + selectedFloor + "&interval=" + Constants.DEFAULT_OCCASION_MINUTE;
        } else {
            Constants.FROM_OCCASION_LIST = false;
        }

        if (selectedFloor == 0) {
            fabChange.performClick();
            return;
        }

        //mRepository.GetFloors("token", buildingId);
        loadURL();
    }

    private void loadURL() {
        myWebView.loadUrl(URL);
        myWebView.clearCache(false);
        cookieManager.setAcceptThirdPartyCookies(myWebView, true);

        myWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                String cookies = CookieManager.getInstance().getCookie(url);
                Log.d("WEBWEBWEB", "All the cookies in a string:" + cookies);
                cookieSyncManager.sync();
                myWebView.clearHistory();


            }
        });
    }

    public void doFloorListWorks(List<Floor> floorList) {
        mFloorList = floorList;
        List<String> floors = stream(floorList).select(Floor::getName).toList();
        floors.add(0, " Select...");
        spinnerFloors.setAdapter(new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, floors));
        spinnerFloors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the value selected by the user
                // e.g. to store it as a field or immediately call a method
                if (position != 0) {
                    selectedFloor = Integer.parseInt(mFloorList.get(position - 1).getID());
                    URL = "http://85.105.163.29/backend/plugins/mapplic/main/2apartment.php?floorId=" + selectedFloor + "&interval=" + SELECTED_BOOK_MIN;
                    loadURL();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        myWebView.saveState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myWebView.restoreState(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CUSTOM_OFFICE) {
            if (resultCode == RESULT_OK) {
                URL = "http://85.105.163.29/backend/plugins/mapplic/main/2apartment.php?floorId=" + data.getIntExtra("floorId", 0) + "&interval=" + SELECTED_BOOK_MIN;
                loadURL();
/*
                mSelectedCampusId = data.getIntExtra("campusId", 0);
                mSelectedBuildingId = data.getIntExtra("buildingId", 0);
                mSelectedFloorId = data.getIntExtra("floorId", 0);

                findViewById(R.id.tv_location_myOffice).setTag(false);
                setBookrenColor(findViewById(R.id.tv_location_myOffice), true);

                findViewById(R.id.tv_location_myOffice).setTag(true);
                setBookrenColor(findViewById(R.id.tv_location_myOffice), true);

                findViewById(R.id.tv_location_nearby).setTag(true);
                setBookrenColor(findViewById(R.id.tv_location_nearby), true);*/
            } else {
                URL = "http://85.105.163.29/backend/plugins/mapplic/main/2apartment.php?floorId=" + selectedFloor + "&interval=" + SELECTED_BOOK_MIN;
            }
        }
    }
}
