package com.unisoft.bookreen1.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Login;
import com.unisoft.bookreen1.network.Repository;

public class LoginActivity extends Activity {
    private ProgressBar mProgressBar;
    private TextView mTvError;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    999);
        }

        setContentView(R.layout.activity_login);
        EditText editText = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);
        mTvError = findViewById(R.id.tv_error);
        mProgressBar = findViewById(R.id.pb_login);

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTvError.setVisibility(View.GONE);

                if (editText.getText().toString().equals("") || password.getText().toString().equals("")) {
                    mTvError.setVisibility(View.VISIBLE);
                    mTvError.setText("Please fill the blanks.");
                    return;
                }

                mProgressBar.setVisibility(View.VISIBLE);
                new Repository(LoginActivity.this, LoginActivity.this).Login("token", editText.getText().toString(), password.getText().toString());
                //RestClient.restApiInterface.


/*                NetworkRequest.performAsyncRequest(RestClient.restApiInterface.GetBuildings(4),
                        (data) -> {
                        }, (error) -> {
                            // Handle Error
                            Log.e("asd", "addPatient Error: " + error.toString());
                        });*/

            }
        });
    }


    public void doYourLoginWorks(Login data) {
        mProgressBar.setVisibility(View.GONE);

        if (data.getStatus().equals("1")) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        } else {
            mTvError.setVisibility(View.VISIBLE);
            mTvError.setText("Something went wrong.");
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == 999) {
            }

        }
    }
}
