package com.unisoft.bookreen1.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Occasion;
import com.unisoft.bookreen1.model.Office;
import com.unisoft.bookreen1.model.SearchBooking;
import com.unisoft.bookreen1.network.Repository;
import com.unisoft.bookreen1.util.MyPreferences;

import java.util.ArrayList;
import java.util.List;

public class OccasionsActivity extends AppCompatActivity {
    private RecyclerView.Adapter mAdapter;
    private List<Occasion> mOccasions;
    private RecyclerView occasionRecycler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        occasionRecycler = findViewById(R.id.rec_occasions);
        occasionRecycler.setHasFixedSize(true);
        occasionRecycler.setLayoutManager(new LinearLayoutManager(this));
        int campusId = MyPreferences.GetCampusPref(this);
        int buildingId = MyPreferences.GetBuildingPref(this);
        int floorId = MyPreferences.GetFloorPref(this);


        List<Integer> equipments = new ArrayList<>();


        List<Integer> layoutIds = new ArrayList<>();

        SearchBooking searchBooking = new SearchBooking();
        searchBooking.setCapacity(10);
        searchBooking.setPlanDate("2020-07-07");
        searchBooking.setEquipmentIds(equipments);
        searchBooking.setLayoutId(102);
        searchBooking.setSTime("12:00");
        searchBooking.setETime("15:00");
        searchBooking.setLocationType(2);
        searchBooking.setOffice(new Office(campusId, buildingId, floorId));


        new Repository(OccasionsActivity.this, OccasionsActivity.this).SearchBooking("token",searchBooking);
    }


    private void setRecycleAdapter(List<Occasion> upcomingEvents) {
        mAdapter.setHasStableIds(true);
        occasionRecycler.setAdapter(mAdapter);
    }

    public void doGetRoomsAndDesksWorks(List<Occasion> data) {
        mOccasions = data;
        setRecycleAdapter(mOccasions);
    }
}