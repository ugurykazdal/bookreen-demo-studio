package com.unisoft.bookreen1.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.UpcomingEvent;
import com.unisoft.bookreen1.network.Repository;
import com.unisoft.bookreen1.ui.adapter.UpcomingEventAdapter;
import com.unisoft.bookreen1.util.Constants;
import com.unisoft.bookreen1.util.NullUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static br.com.zbra.androidlinq.Linq.stream;

public class HomeFragment extends Fragment {
    private RecyclerView.Adapter mAdapter;
    private List<UpcomingEvent> mUpcomingEvents;
    private RecyclerView upcommingRecycler;
    private View mRootView;
    private ImageView imgMin15, imgMin30, imgMin45, imgHour1, imgOther;
    private final int REQUEST_OTHER_DATE = 113;
    private ProgressBar pb_upcoming;
    private LinearLayout ly_noContent;
    private Spinner mSpn_eventFilter;

    public static final int FILTER_ALL = 0;
    public static final int FILTER_ACTIVE = 1;
    public static final int FILTER_UPCOMING = 2;

    private int mSelectedFilter = FILTER_ALL;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRootView = view;

        pb_upcoming = view.findViewById(R.id.pb_upcomingevents);
        ly_noContent = view.findViewById(R.id.ly_nocontentEvents);

        upcommingRecycler = view.findViewById(R.id.rec_upcoming);
        upcommingRecycler.setHasFixedSize(true);
        upcommingRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mSpn_eventFilter = view.findViewById(R.id.spn_home_filter);

        List<String> buildings = Arrays.asList(getResources().getStringArray(R.array.event_filter));
        mSpn_eventFilter.setAdapter(new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, buildings));
        mSpn_eventFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the value selected by the user
                // e.g. to store it as a field or immediately call a method

                mSelectedFilter = position;

                new Repository(getContext(), HomeFragment.this).GetUpcomingEvents("token");


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        view.findViewById(R.id.img_scroll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                upcommingRecycler.scrollToPosition(getLastPosition());

            }
        });

        imgMin15 = view.findViewById(R.id.img_event_min15);
        imgMin30 = view.findViewById(R.id.img_event_min30);
        imgMin45 = view.findViewById(R.id.img_event_min45);
        imgHour1 = view.findViewById(R.id.img_event_hour1);
        imgOther = view.findViewById(R.id.img_event_other);

        mRootView.findViewById(R.id.img_home_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MyProfileActivity.class));
            }
        });

        pb_upcoming.setVisibility(View.VISIBLE);
        upcommingRecycler.setVisibility(View.GONE);
        ly_noContent.setVisibility(View.GONE);

        imgMin15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() == null || !((boolean) view.getTag())) {
                    view.setTag(true);
                    imgMin15.setImageResource(R.drawable.min15_secili);
                } else {
                    view.setTag(false);
                    imgMin15.setImageResource(R.drawable.min15);
                }

                imgOther.setImageResource(R.drawable.other);
                imgOther.setTag(false);
                imgHour1.setImageResource(R.drawable.hour1);
                imgHour1.setTag(false);
                imgMin45.setImageResource(R.drawable.min45);
                imgMin45.setTag(false);
                imgMin30.setImageResource(R.drawable.min30);
                imgMin30.setTag(false);
                Constants.SELECTED_BOOK_MIN = 15;
                Constants.OTHER_BOOK_SELECTED = false;
                Constants.mainActivity.occasionFragment = new OccasionFragment();
                Constants.mainActivity.loadFragment(Constants.mainActivity.occasionFragment);
                Constants.mainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_add);


            }
        });

        imgMin30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() == null || !((boolean) view.getTag())) {
                    view.setTag(true);
                    imgMin30.setImageResource(R.drawable.min30_secili);
                } else {
                    view.setTag(false);
                    imgMin30.setImageResource(R.drawable.min30);
                }
                imgOther.setImageResource(R.drawable.other);
                imgOther.setTag(false);
                imgHour1.setImageResource(R.drawable.hour1);
                imgHour1.setTag(false);
                imgMin45.setImageResource(R.drawable.min45);
                imgMin45.setTag(false);
                imgMin15.setImageResource(R.drawable.min15);
                imgMin15.setTag(false);

                Constants.SELECTED_BOOK_MIN = 30;
                Constants.OTHER_BOOK_SELECTED = false;

                Constants.mainActivity.occasionFragment = new OccasionFragment();
                Constants.mainActivity.loadFragment(Constants.mainActivity.occasionFragment);
                Constants.mainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_add);
            }
        });

        imgMin45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() == null || !((boolean) view.getTag())) {
                    view.setTag(true);
                    imgMin45.setImageResource(R.drawable.min45_secili);
                } else {
                    view.setTag(false);
                    imgMin45.setImageResource(R.drawable.min45);
                }

                imgOther.setImageResource(R.drawable.other);
                imgOther.setTag(false);
                imgHour1.setImageResource(R.drawable.hour1);
                imgHour1.setTag(false);
                imgMin30.setImageResource(R.drawable.min30);
                imgMin30.setTag(false);
                imgMin15.setImageResource(R.drawable.min15);
                imgMin15.setTag(false);

                Constants.SELECTED_BOOK_MIN = 45;
                Constants.OTHER_BOOK_SELECTED = false;

                Constants.mainActivity.occasionFragment = new OccasionFragment();
                Constants.mainActivity.loadFragment(Constants.mainActivity.occasionFragment);
                Constants.mainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_add);
            }
        });

        imgHour1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() == null || !((boolean) view.getTag())) {
                    view.setTag(true);
                    imgHour1.setImageResource(R.drawable.hour1_secili);
                } else {
                    view.setTag(false);
                    imgHour1.setImageResource(R.drawable.hour1);
                }

                imgOther.setImageResource(R.drawable.other);
                imgOther.setTag(false);
                imgMin45.setImageResource(R.drawable.min45);
                imgMin45.setTag(false);
                imgMin30.setImageResource(R.drawable.min30);
                imgMin30.setTag(false);
                imgMin15.setImageResource(R.drawable.min15);
                imgMin15.setTag(false);

                Constants.SELECTED_BOOK_MIN = 60;
                Constants.OTHER_BOOK_SELECTED = false;

                Constants.mainActivity.occasionFragment = new OccasionFragment();
                Constants.mainActivity.loadFragment(Constants.mainActivity.occasionFragment);
                Constants.mainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_add);
            }
        });

        imgOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getTag() == null || !((boolean) view.getTag())) {
                    view.setTag(true);
                    imgOther.setImageResource(R.drawable.other_secim);
                } else {
                    view.setTag(false);
                    imgOther.setImageResource(R.drawable.other);
                }

                imgHour1.setImageResource(R.drawable.hour1);
                imgHour1.setTag(false);
                imgMin45.setImageResource(R.drawable.min45);
                imgMin45.setTag(false);
                imgMin30.setImageResource(R.drawable.min30);
                imgMin30.setTag(false);
                imgMin15.setImageResource(R.drawable.min15);
                imgMin15.setTag(false);

                Constants.SELECTED_BOOK_MIN = 0;
                startActivityForResult(new Intent(getContext(), FindDateSelectActivity.class), REQUEST_OTHER_DATE);
            }
        });
    }

    private void setRecycleAdapter(List<UpcomingEvent> upcomingEvents) {
        mAdapter = new UpcomingEventAdapter(upcomingEvents);
        mAdapter.setHasStableIds(true);
        upcommingRecycler.setAdapter(mAdapter);
    }

    int getLastPosition() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) upcommingRecycler.getLayoutManager());
        int pos = layoutManager.findLastCompletelyVisibleItemPosition();
        return pos + 1;

    }

    public void doUpcomingEventsWorks(List<UpcomingEvent> data) {
        mUpcomingEvents = data;
        if (data == null || data.isEmpty()) {
            ly_noContent.setVisibility(View.VISIBLE);
        } else {
            ly_noContent.setVisibility(View.GONE);
        }

        switch (mSelectedFilter) {
            case FILTER_ALL:
                mUpcomingEvents = stream(mUpcomingEvents).where(i -> i.getStatus() == 1 || i.getStatus() == 8).toList();
                break;
            case FILTER_ACTIVE:
                mUpcomingEvents = stream(mUpcomingEvents).where(i -> i.getStartDate().getTime() <= new Date().getTime() - 1000).toList();
                break;
            case FILTER_UPCOMING:
                mUpcomingEvents = stream(mUpcomingEvents).where(i -> i.getStartDate().getTime() >= new Date().getTime() - 1000).toList();
                break;
        }

        setRecycleAdapter(mUpcomingEvents);
        pb_upcoming.setVisibility(View.GONE);
        upcommingRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_OTHER_DATE) {
            if (resultCode == RESULT_OK) {
                Constants.SELECTED_BOOK_MIN = data.getIntExtra("min", 0);

                Constants.SELECTED_BOOK_DATE = data.getStringExtra("selectedDate");
                Constants.SELECTED_BOOK_STIME = data.getStringExtra("selectedSTime");
                Constants.SELECTED_BOOK_ETIME = data.getStringExtra("selectedEDate");

                Constants.OTHER_BOOK_SELECTED = true;

                Constants.mainActivity.occasionFragment = new OccasionFragment();
                Constants.mainActivity.loadFragment(Constants.mainActivity.occasionFragment);
                Constants.mainActivity.bottomNavigationView.setSelectedItemId(R.id.navigation_add);
            } else {
                Constants.OTHER_BOOK_SELECTED = false;
                imgOther.setTag(false);
                imgOther.setImageResource(R.drawable.other);
            }
        }
    }

    public boolean isAnyMinuteSelected() {
        return NullUtil.isTagTrue(imgMin15) || NullUtil.isTagTrue(imgMin30) || NullUtil.isTagTrue(imgMin45) || NullUtil.isTagTrue(imgHour1) || NullUtil.isTagTrue(imgOther);
    }

}