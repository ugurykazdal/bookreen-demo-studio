package com.unisoft.bookreen1.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.model.Building;
import com.unisoft.bookreen1.model.Campus;
import com.unisoft.bookreen1.model.Floor;
import com.unisoft.bookreen1.model.Office;
import com.unisoft.bookreen1.network.Repository;
import com.unisoft.bookreen1.util.MyPreferences;

import java.util.List;

import static br.com.zbra.androidlinq.Linq.stream;

public class CustomOfficeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private int selectedCampus = 9999;
    private int selectedBuilding = 9999;
    private int selectedFloor = 9999;

    private List<Campus> mCampusList;
    private List<Building> mBuildingList;
    private List<Floor> mFloorList;
    private Spinner spinnerCampus;
    private Spinner spinnerBuildings;
    private Spinner spinnerFloors;
    private Repository mRepository;
    private Office mCustomOffice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvitiy_custom_office);
        mRepository = new Repository(CustomOfficeActivity.this, CustomOfficeActivity.this);

        spinnerCampus = (Spinner) findViewById(R.id.spn_customLoc_campus);
        spinnerBuildings = (Spinner) findViewById(R.id.spn_customLoc_building);
        spinnerFloors = (Spinner) findViewById(R.id.spn_customLoc_floor);

        mCustomOffice = MyPreferences.GetCustomOffice(CustomOfficeActivity.this);


        mRepository.GetCampuses("token");

        findViewById(R.id.btn_customLoc_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("campusId", selectedCampus);
                intent.putExtra("buildingId", selectedBuilding);
                intent.putExtra("floorId", selectedFloor);

                MyPreferences.SaveCustomCampusPref(CustomOfficeActivity.this, selectedCampus);
                MyPreferences.SaveCustomBuildingPref(CustomOfficeActivity.this, selectedBuilding);
                MyPreferences.SaveCustomFloorPref(CustomOfficeActivity.this, selectedFloor);

                setResult(RESULT_OK, intent);
                finish();

            }
        });

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        switch ((int) parent.getTag()) {
            case 1:
                selectedCampus = pos;
                break;
            case 2:
                selectedBuilding = pos;
                break;
            case 3:
                selectedFloor = pos;
                break;
        }
        Log.i("My Selected", selectedCampus + " : " + selectedBuilding + " : " + selectedFloor);

        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void doCampusListWorks(List<Campus> campusList) {
        mCampusList = campusList;
        List<String> campuses = stream(campusList).select(Campus::getName).toList();
        campuses.add(0, "Campus");
        spinnerCampus.setAdapter(new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, campuses));
        spinnerCampus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the value selected by the user
                // e.g. to store it as a field or immediately call a method
                if (position != 0) {
                    spinnerBuildings.setAdapter(null);
                    spinnerFloors.setAdapter(null);
                    selectedBuilding = 999;
                    selectedFloor = 999;
                    selectedCampus = Integer.parseInt(mCampusList.get(position - 1).getID());
                    MyPreferences.SaveCustomCampusPref(CustomOfficeActivity.this, selectedCampus);

                    mRepository.GetBuildings("token", selectedCampus);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        if (mCustomOffice.getCampusID() != 0) {
            Campus campus = stream(mCampusList).where(i -> Integer.parseInt(i.getID()) == mCustomOffice.getCampusID()).firstOrNull();

            if (campus != null) {
                int indeks = mCampusList.indexOf(campus);
                spinnerCampus.setSelection(indeks + 1);
            }
        }
    }

    public void doBuildingListWorks(List<Building> buildingList) {
        mBuildingList = buildingList;
        List<String> buildings = stream(buildingList).select(Building::getName).toList();
        buildings.add(0, "Building");
        spinnerBuildings.setAdapter(new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, buildings));
        spinnerBuildings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the value selected by the user
                // e.g. to store it as a field or immediately call a method
                if (position != 0) {
                    selectedBuilding = Integer.parseInt(mBuildingList.get(position - 1).getID());
                    MyPreferences.SaveCustomBuildingPref(CustomOfficeActivity.this, selectedBuilding);

                    mRepository.GetFloors("token", selectedBuilding);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        if (mCustomOffice.getBuildingID() != 0) {
            Building building = stream(mBuildingList).where(i -> Integer.parseInt(i.getID()) == mCustomOffice.getBuildingID()).firstOrNull();

            if (building != null) {
                int indeks = mBuildingList.indexOf(building);
                spinnerBuildings.setSelection(indeks + 1);
            }
        }
    }

    public void doFloorListWorks(List<Floor> floorList) {
        mFloorList = floorList;
        List<String> floors = stream(floorList).select(Floor::getName).toList();
        floors.add(0, "Floor");
        spinnerFloors.setAdapter(new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, floors));
        spinnerFloors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the value selected by the user
                // e.g. to store it as a field or immediately call a method
                if (position != 0) {
                    selectedFloor = Integer.parseInt(mFloorList.get(position - 1).getID());
                    MyPreferences.SaveCustomFloorPref(CustomOfficeActivity.this, selectedFloor);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        if (mCustomOffice.getFloorID() != 0) {
            Floor floor = stream(mFloorList).where(i -> Integer.parseInt(i.getID()) == mCustomOffice.getFloorID()).firstOrNull();

            if (floor != null) {
                int indeks = mFloorList.indexOf(floor);
                spinnerFloors.setSelection(indeks + 1);
            }
        }
    }
}