package com.unisoft.bookreen1.ui.adapter;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.unisoft.bookreen1.R;
import com.unisoft.bookreen1.ui.HomeFragment;
import com.unisoft.bookreen1.util.DateUtil;
import com.unisoft.bookreen1.model.UpcomingEvent;

import java.util.List;

public class UpcomingEventAdapter extends RecyclerView.Adapter<UpcomingEventAdapter.MyViewHolder> {
    private List<UpcomingEvent> mDataset;
    View mView;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tv_mahal;
        TextView tv_subject;
        TextView tv_duzenleyen;
        TextView tv_date;

        MyViewHolder(View v) {
            super(v);
            tv_mahal = v.findViewById(R.id.tv_eventMahal);
            tv_subject = v.findViewById(R.id.tv_eventSubject);
            tv_duzenleyen = v.findViewById(R.id.tv_duzenleyen);
            tv_date = v.findViewById(R.id.tv_date);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public UpcomingEventAdapter(List<UpcomingEvent> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public UpcomingEventAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view
        mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_events, parent, false);
        return new MyViewHolder(mView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tv_mahal.setText(mDataset.get(position).getLocationName());
        holder.tv_subject.setText(mDataset.get(position).getSubject());
        try {
            holder.tv_date.setText(mDataset.get(position).getSTime().substring(0, 5) + " : " + mDataset.get(position).getETime().substring(0, 5) + " , " + DateUtil.formatUpcomingDates(mDataset.get(position).getPlanDate()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tv_duzenleyen.setText(mDataset.get(position).getOrganizer());

        if (mDataset.get(position).getInStatus() == HomeFragment.FILTER_ACTIVE)
            mView.setBackgroundTintList(ColorStateList.valueOf(mView.getContext().getResources().getColor(R.color.yellow_50)));
        else
            mView.setBackgroundTintList(ColorStateList.valueOf(mView.getContext().getResources().getColor(R.color.green_50)));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void scrollToposition() {

    }
}