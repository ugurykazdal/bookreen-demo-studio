package com.unisoft.bookreen1.network;

import com.unisoft.bookreen1.model.Reservation;
import com.unisoft.bookreen1.model.SearchBooking;

public interface IRepository {
    void Login(String token, String username, String password);

    void GetCampuses(String token);

    void GetBuildings(String token, int campusId);

    void GetFloors(String token, int buildingId);

    void GetUpcomingEvents(String token);

    void SearchBooking(String token, SearchBooking searchBooking);

    void SaveReservation(String token, Reservation reservation);

    void SaveMyOffice(int campusId, int buildingId, int floorId);

}
