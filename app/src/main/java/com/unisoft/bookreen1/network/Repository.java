package com.unisoft.bookreen1.network;

import android.content.Context;
import android.util.Log;

import com.unisoft.bookreen1.model.Reservation;
import com.unisoft.bookreen1.model.SearchBooking;
import com.unisoft.bookreen1.network.rest.NetworkRequest;
import com.unisoft.bookreen1.network.rest.RestClient;
import com.unisoft.bookreen1.ui.CustomOfficeActivity;
import com.unisoft.bookreen1.ui.HomeFragment;
import com.unisoft.bookreen1.ui.LoginActivity;
import com.unisoft.bookreen1.ui.MyOfficeActivity;
import com.unisoft.bookreen1.ui.OccasionFragment;
import com.unisoft.bookreen1.ui.OccasionsActivity;
import com.unisoft.bookreen1.ui.PinFragment;
import com.unisoft.bookreen1.ui.TeyitActivity;

public class Repository implements IRepository {
    private Context mContext;
    private Object mObject;

    public Repository(Context _context, Object _object) {
        this.mContext = _context;
        this.mObject = _object;
    }

    @Override
    public void Login(String token, String username, String password) {
        NetworkRequest.performAsyncRequest(RestClient.restApiInterface.Login(username, password),
                (data) -> {
                    if (mObject instanceof LoginActivity) {
                        ((LoginActivity) mObject).doYourLoginWorks(data);
                    }
                }, (error) -> {
                    // Handle Error
                    Log.e("LOGIN ERROR: ", error.toString());
                });
    }

    @Override
    public void GetCampuses(String token) {
        NetworkRequest.performAsyncRequest(RestClient.restApiInterface.GetCampuses(token),
                (data) -> {
                    if (mObject instanceof MyOfficeActivity) {
                        ((MyOfficeActivity) mObject).doCampusListWorks(data);
                    }
                    if (mObject instanceof CustomOfficeActivity) {
                        ((CustomOfficeActivity) mObject).doCampusListWorks(data);
                    }
                }, (error) -> {

                    // Handle Error
                    Log.e("GETCAMPUSES ERROR: ", error.toString());

                });
    }

    @Override
    public void GetBuildings(String token, int campusId) {
        NetworkRequest.performAsyncRequest(RestClient.restApiInterface.GetBuildings(token, campusId),
                (data) -> {
                    if (mObject instanceof MyOfficeActivity) {
                        ((MyOfficeActivity) mObject).doBuildingListWorks(data);
                    }
                    if (mObject instanceof CustomOfficeActivity) {
                        ((CustomOfficeActivity) mObject).doBuildingListWorks(data);
                    }
                }, (error) -> {
                    // Handle Error
                    Log.e("BUILDING ERROR", error.toString());
                });
    }

    @Override
    public void GetFloors(String token, int buildingId) {
        NetworkRequest.performAsyncRequest(RestClient.restApiInterface.GetFloors(token, buildingId),
                (data) -> {
                    if (mObject instanceof MyOfficeActivity) {
                        ((MyOfficeActivity) mObject).doFloorListWorks(data);
                    }
                    if (mObject instanceof CustomOfficeActivity) {
                        ((CustomOfficeActivity) mObject).doFloorListWorks(data);
                    }
                    if (mObject instanceof PinFragment) {
                        ((PinFragment) mObject).doFloorListWorks(data);
                    }
                }, (error) -> {
                    // Handle Error
                    Log.e("FLOOR ERROR: ", error.toString());
                });
    }

    @Override
    public void GetUpcomingEvents(String token) {
        NetworkRequest.performAsyncRequest(RestClient.restApiInterface.GetUpcomingEvents(token),
                (data) -> {
                    if (mObject instanceof HomeFragment) {
                        ((HomeFragment) mObject).doUpcomingEventsWorks(data);
                    }

                }, (error) -> {
                    Log.e("UPCOMING EVENTS ERROR: ", error.toString());

                });
    }

    @Override
    public void SearchBooking(String token, SearchBooking searchBooking) {

        NetworkRequest.performAsyncRequest(RestClient.restApiInterface.SearchBooking(token, searchBooking),
                (data) -> {
                    if (mObject instanceof OccasionFragment) {
                        ((OccasionFragment) mObject).doGetRoomsAndDesksWorks(data);
                    }
                    if (mObject instanceof OccasionsActivity) {
                        ((OccasionsActivity) mObject).doGetRoomsAndDesksWorks(data);
                    }

                }, (error) -> {

                    Log.e("ROOM DESK LIST ERROR: ", error.toString());

                });
    }

    @Override
    public void SaveReservation(String token, Reservation reservation) {
        NetworkRequest.performAsyncRequest(RestClient.restApiInterface.SaveReservation("token", reservation),
                (data) -> {
            if(mObject instanceof TeyitActivity){
                ((TeyitActivity)mObject).doSaveReservationWorks(data);
            }
                    Log.v("asd", data.toString());

                }, (error) -> {

                    // Handle Error
                    Log.e("asd", "error " + error.toString());

                });
    }


    @Override
    public void SaveMyOffice(int campusId, int buildingId, int floorId) {

    }
}
