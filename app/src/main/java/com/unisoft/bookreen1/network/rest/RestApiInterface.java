package com.unisoft.bookreen1.network.rest;


import androidx.annotation.NonNull;


import com.unisoft.bookreen1.model.Building;
import com.unisoft.bookreen1.model.Floor;
import com.unisoft.bookreen1.model.Campus;
import com.unisoft.bookreen1.model.Login;
import com.unisoft.bookreen1.model.Occasion;
import com.unisoft.bookreen1.model.Reservation;
import com.unisoft.bookreen1.model.SearchBooking;
import com.unisoft.bookreen1.model.UpcomingEvent;
import com.unisoft.bookreen1.network.Response;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface RestApiInterface {

    @NonNull
    @POST("/mobile/login")
    Observable<Login> Login(@Query("LoginName") String loginName, @Query("Password") String password);

    @NonNull
    @GET("/mobile/campuses")
    Observable<List<Campus>> GetCampuses(@Query("Token") String token);

    @NonNull

    @GET("/mobile/buildings")
    Observable<List<Building>> GetBuildings(@Query("Token") String token, @Query("CampusID") int campusId);

    @NonNull
    @GET("/mobile/floors")
    Observable<List<Floor>> GetFloors(@Query("Token") String token, @Query("BuildingID") int buildingId);

    @NonNull
    @POST("/mobile/getUpcomingEvents")
    Observable<List<UpcomingEvent>> GetUpcomingEvents(@Query("Token") String token);

    @NonNull
    @POST("/mobile/searchBooking")
    Observable<List<Occasion>> SearchBooking(@Query("Token") String token, @Body SearchBooking searchBooking);

    @NonNull
    @POST("/mobile/saveReservation")
    Observable<Response<Reservation>> SaveReservation(@Query("Token") String token, @Body Reservation reservation);

    @NonNull
    @POST("/mobile/saveMyOffice")
    Observable<Object> SaveMyOffice(@Query("Token") String token, @Query("CampusID") int campusID, @Query("BuildingID") int buildingID, @Query("FloorID") int floorID);

    @NonNull
    @POST("https://www.googleapis.com/calendar/v3/calendars/calendarId/events")
    Observable<Object> GetCalendars();

    @NonNull
    @POST("https://www.googleapis.com/calendar/v3/calendars/uguryasarkazdal@gmail.com/events?key=411529648858-ere28d5rjpauf941g78njac4p2sdo37c.apps.googleusercontent.com")
    Observable<Object> TOKEN();

}