package com.unisoft.bookreen1.network.rest;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unisoft.bookreen1.network.extra.JavaNetCookieJar;
import com.unisoft.bookreen1.network.extra.JsonDateDeserializer;
import com.unisoft.bookreen1.network.extra.LoggingInterceptor;

import java.net.CookieManager;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    public static RestApiInterface restApiInterface;

    public RestClient() {
        restApiInterface = providedRestApiInterface(provideWeatherRetrofit(provideGson(), provideOkHttpClient(providedHttpInterceptor())));
    }

    public final HttpLoggingInterceptor providedHttpInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    public final Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .setLenient()
                .setPrettyPrinting()
                .registerTypeAdapter(Date.class, new JsonDateDeserializer())
                .create();
    }

    public final OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(new LoggingInterceptor())
                .cookieJar(new JavaNetCookieJar(new CookieManager()))
                .addNetworkInterceptor(new StethoInterceptor())
                .connectTimeout(90L, TimeUnit.SECONDS)
                .writeTimeout(90L, TimeUnit.SECONDS)
                .readTimeout(90L, TimeUnit.SECONDS)
                .build();


    }


    public final Retrofit provideWeatherRetrofit(Gson gson, OkHttpClient okHttpClient) {

        return
                new Retrofit.Builder()
                        .callbackExecutor(Executors.newSingleThreadExecutor())
                           .baseUrl("http://85.105.163.29/")
                        //  .baseUrl("http://192.168.1.14")
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(okHttpClient)
                        .build();
    }


    public final RestApiInterface providedRestApiInterface(Retrofit retrofit) {
        return retrofit.create(RestApiInterface.class);
    }
}
