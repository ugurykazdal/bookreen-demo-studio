package com.unisoft.bookreen1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Occasion {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("CampusName")
    @Expose
    private String campusName;
    @SerializedName("BuildingName")
    @Expose
    private String buildingName;
    @SerializedName("FloorName")
    @Expose
    private String floorName;
    @SerializedName("Equipment")
    @Expose
    private Object equipment;
    @SerializedName("Services")
    @Expose
    private Object services;
    @SerializedName("Layout")
    @Expose
    private Integer layout;
    @SerializedName("Capacity")
    @Expose
    private String capacity;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCampusName() {
        return campusName;
    }

    public void setCampusName(String campusName) {
        this.campusName = campusName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }

    public Object getEquipment() {
        return equipment;
    }

    public void setEquipment(Object equipment) {
        this.equipment = equipment;
    }

    public Object getServices() {
        return services;
    }

    public void setServices(Object services) {
        this.services = services;
    }


    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public Integer getLayout() {
        return layout;
    }

    public void setLayout(Integer layout) {
        this.layout = layout;
    }
}