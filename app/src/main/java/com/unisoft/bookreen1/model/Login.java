package com.unisoft.bookreen1.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

@SerializedName("ID")
@Expose
private String iD;
@SerializedName("LoginName")
@Expose
private String loginName;
@SerializedName("Password")
@Expose
private String password;
@SerializedName("Name")
@Expose
private String name;
@SerializedName("Surname")
@Expose
private String surname;
@SerializedName("Explanation")
@Expose
private String explanation;
@SerializedName("PersonID")
@Expose
private Object personID;
@SerializedName("DepartmentID")
@Expose
private String departmentID;
@SerializedName("CompanyID")
@Expose
private String companyID;
@SerializedName("LoginCount")
@Expose
private Object loginCount;
@SerializedName("Status")
@Expose
private String status;
@SerializedName("CDate")
@Expose
private String cDate;
@SerializedName("MDate")
@Expose
private String mDate;
@SerializedName("CUser")
@Expose
private String cUser;
@SerializedName("MUser")
@Expose
private String mUser;
@SerializedName("EMail")
@Expose
private String eMail;
@SerializedName("Phone")
@Expose
private String phone;
@SerializedName("Question")
@Expose
private String question;
@SerializedName("Answer")
@Expose
private String answer;
@SerializedName("Language")
@Expose
private String language;
@SerializedName("ProfilePhoto")
@Expose
private Object profilePhoto;
@SerializedName("PIN")
@Expose
private Object pIN;
@SerializedName("NFCID")
@Expose
private Object nFCID;

public String getID() {
return iD;
}

public void setID(String iD) {
this.iD = iD;
}

public String getLoginName() {
return loginName;
}

public void setLoginName(String loginName) {
this.loginName = loginName;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getSurname() {
return surname;
}

public void setSurname(String surname) {
this.surname = surname;
}

public String getExplanation() {
return explanation;
}

public void setExplanation(String explanation) {
this.explanation = explanation;
}

public Object getPersonID() {
return personID;
}

public void setPersonID(Object personID) {
this.personID = personID;
}

public String getDepartmentID() {
return departmentID;
}

public void setDepartmentID(String departmentID) {
this.departmentID = departmentID;
}

public String getCompanyID() {
return companyID;
}

public void setCompanyID(String companyID) {
this.companyID = companyID;
}

public Object getLoginCount() {
return loginCount;
}

public void setLoginCount(Object loginCount) {
this.loginCount = loginCount;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getCDate() {
return cDate;
}

public void setCDate(String cDate) {
this.cDate = cDate;
}

public String getMDate() {
return mDate;
}

public void setMDate(String mDate) {
this.mDate = mDate;
}

public String getCUser() {
return cUser;
}

public void setCUser(String cUser) {
this.cUser = cUser;
}

public String getMUser() {
return mUser;
}

public void setMUser(String mUser) {
this.mUser = mUser;
}

public String getEMail() {
return eMail;
}

public void setEMail(String eMail) {
this.eMail = eMail;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public String getQuestion() {
return question;
}

public void setQuestion(String question) {
this.question = question;
}

public String getAnswer() {
return answer;
}

public void setAnswer(String answer) {
this.answer = answer;
}

public String getLanguage() {
return language;
}

public void setLanguage(String language) {
this.language = language;
}

public Object getProfilePhoto() {
return profilePhoto;
}

public void setProfilePhoto(Object profilePhoto) {
this.profilePhoto = profilePhoto;
}

public Object getPIN() {
return pIN;
}

public void setPIN(Object pIN) {
this.pIN = pIN;
}

public Object getNFCID() {
return nFCID;
}

public void setNFCID(Object nFCID) {
this.nFCID = nFCID;
}

}