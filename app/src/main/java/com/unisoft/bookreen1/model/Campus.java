package com.unisoft.bookreen1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Campus {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("CUser")
    @Expose
    private String cUser;
    @SerializedName("CDate")
    @Expose
    private String cDate;
    @SerializedName("MUser")
    @Expose
    private Object mUser;
    @SerializedName("MDate")
    @Expose
    private Object mDate;
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("TimeZoneID")
    @Expose
    private String timeZoneID;
    @SerializedName("ImageID")
    @Expose
    private String imageID;
    @SerializedName("ScreenLanguage")
    @Expose
    private String screenLanguage;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCUser() {
        return cUser;
    }

    public void setCUser(String cUser) {
        this.cUser = cUser;
    }

    public String getCDate() {
        return cDate;
    }

    public void setCDate(String cDate) {
        this.cDate = cDate;
    }

    public Object getMUser() {
        return mUser;
    }

    public void setMUser(Object mUser) {
        this.mUser = mUser;
    }

    public Object getMDate() {
        return mDate;
    }

    public void setMDate(Object mDate) {
        this.mDate = mDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTimeZoneID() {
        return timeZoneID;
    }

    public void setTimeZoneID(String timeZoneID) {
        this.timeZoneID = timeZoneID;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public String getScreenLanguage() {
        return screenLanguage;
    }

    public void setScreenLanguage(String screenLanguage) {
        this.screenLanguage = screenLanguage;
    }

}