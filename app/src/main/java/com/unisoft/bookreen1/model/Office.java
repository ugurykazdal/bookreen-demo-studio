package com.unisoft.bookreen1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Office {
    @SerializedName("CampusID")
    @Expose
    private Integer campusID;
    @SerializedName("BuildingID")
    @Expose
    private Integer buildingID;
    @SerializedName("FloorID")
    @Expose
    private Integer floorID;

    public Integer getCampusID() {
        return campusID;
    }

    public void setCampusID(Integer campusID) {
        this.campusID = campusID;
    }

    public Integer getBuildingID() {
        return buildingID;
    }

    public void setBuildingID(Integer buildingID) {
        this.buildingID = buildingID;
    }

    public Integer getFloorID() {
        return floorID;
    }

    public void setFloorID(Integer floorID) {
        this.floorID = floorID;
    }

    public Office(Integer campusID, Integer buildingID, Integer floorID) {
        this.campusID = campusID;
        this.buildingID = buildingID;
        this.floorID = floorID;
    }
}