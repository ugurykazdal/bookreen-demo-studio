package com.unisoft.bookreen1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.unisoft.bookreen1.ui.HomeFragment;
import com.unisoft.bookreen1.util.DateUtil;

import java.util.Date;

public class UpcomingEvent {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("STime")
    @Expose
    private String sTime;
    @SerializedName("ETime")
    @Expose
    private String eTime;
    @SerializedName("LocationName")
    @Expose
    private String locationName;
    @SerializedName("Subject")
    @Expose
    private String subject;

    @SerializedName("Organizer")
    @Expose
    private String organizer;

    @SerializedName("PlanDate")
    @Expose
    private String planDate;

    @SerializedName("Status")
    @Expose
    private Integer status;

    private Date endDate;
    private Date startDate;
    private int inStatus;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getSTime() {
        return sTime;
    }

    public void setSTime(String sTime) {
        this.sTime = sTime;
    }

    public String getETime() {
        return eTime;
    }

    public void setETime(String eTime) {
        this.eTime = eTime;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getEndDate() {
        return DateUtil.parseEDate(getPlanDate() + " " + getETime());
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return DateUtil.parseEDate(getPlanDate() + " " + getSTime());
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getInStatus() {
        return getStartDate().getTime() <= new Date().getTime() - 1000 ? HomeFragment.FILTER_ACTIVE : HomeFragment.FILTER_UPCOMING;
    }

    public void setInStatus(int inStatus) {
        this.inStatus = inStatus;
    }
}