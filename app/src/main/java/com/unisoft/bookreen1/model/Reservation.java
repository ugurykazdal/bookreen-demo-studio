package com.unisoft.bookreen1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reservation {

    @SerializedName("LocationID")
    @Expose
    private Integer locationID;
    @SerializedName("UID")
    @Expose
    private Integer uID;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("PlanDate")
    @Expose
    private String date;
    @SerializedName("STime")
    @Expose
    private String sTime;
    @SerializedName("ETime")
    @Expose
    private String eTime;

    public Integer getLocationID() {
        return locationID;
    }

    public void setLocationID(Integer locationID) {
        this.locationID = locationID;
    }

    public Integer getUID() {
        return uID;
    }

    public void setUID(Integer uID) {
        this.uID = uID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSTime() {
        return sTime;
    }

    public void setSTime(String sTime) {
        this.sTime = sTime;
    }

    public String getETime() {
        return eTime;
    }

    public void setETime(String eTime) {
        this.eTime = eTime;
    }

}