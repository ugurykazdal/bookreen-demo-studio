package com.unisoft.bookreen1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchBooking {

    @SerializedName("EquipmentIDs")
    @Expose
    private List<Integer> equipmentIds = null;
    @SerializedName("LayoutID")
    @Expose
    private Integer layoutId = null;
    @SerializedName("Capacity")
    @Expose
    private Integer capacity;
    @SerializedName("PlanDate")
    @Expose
    private String planDate;
    @SerializedName("STime")
    @Expose
    private String sTime;
    @SerializedName("ETime")
    @Expose
    private String eTime;
    @SerializedName("LocationType")
    @Expose
    private Integer locationType;
    @SerializedName("Office")
    @Expose
    private Office office;

    public List<Integer> getEquipmentIds() {
        return equipmentIds;
    }

    public void setEquipmentIds(List<Integer> equipmentIds) {
        this.equipmentIds = equipmentIds;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public String getSTime() {
        return sTime;
    }

    public void setSTime(String sTime) {
        this.sTime = sTime;
    }

    public String getETime() {
        return eTime;
    }

    public void setETime(String eTime) {
        this.eTime = eTime;
    }

    public Integer getLocationType() {
        return locationType;
    }

    public void setLocationType(Integer locationType) {
        this.locationType = locationType;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Integer getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(Integer layoutId) {
        this.layoutId = layoutId;
    }
}